#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>

#include "kcdr.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "conf/conf_kcdr-sender.h"

#define BUFLEN 512

#define KCDR_VERSION	"0.1"

int main (int argc, char **argv) {
	struct kcdr1_flow_header kcdr_fh;
	kcdr_fh.ver = 1;
	kcdr_fh.count = 1;

	struct kcdr1_flow kcdr_f;
	memset((void *) &kcdr_f, 0, sizeof(kcdr_f));

	static char stropts[] = "p:s:d:t:e:u:h:";
	static struct option lopts[] = {
		{"pbx", 1, 0, 'p'},
		{"src-number", 1, 0, 's'},
		{"dst-number", 1, 0, 'd'},
		{"src-trunk", 1, 0, 't'},
		{"dst-trunk", 1, 0, 'e'},
		{"duration", 1, 0, 'u'},
		{"help", 0, 0, 'h'},
		{0, 0, 0, 0}
	};
	int optch = 0, cmdx = 0;

	while ((optch = getopt_long_only(argc, argv, stropts, lopts, &cmdx)) != EOF)
	{
		switch (optch)
		{
	 case 0:
		 break;
	 case 'u':
		kcdr_f.duration = atoi(optarg);
		break;
	 case 'p':
		strcpy(kcdr_f.pbx, optarg);
		break;
	 case 's':
		strcpy(kcdr_f.src_number, optarg);
		break;
	 case 'd':
		strcpy(kcdr_f.dst_number, optarg);
		break;
	 case 't':
		strcpy(kcdr_f.src_trunk, optarg);
		break;
	 case 'e':
		strcpy(kcdr_f.dst_trunk, optarg);
		break;
	 case 'h':
		printf(
"kcdr-sender v%s\n"
"Send UDP packet with kcdr\n"
"usage: kcdr-sender [--pbx <pbx>] [--src-number <number>] [--dst-number <number>] [--src-trunk <trunk>] [--dst-trunk <trunk>] [--duration <duration>]\n"
"\nShow this help\n"
"usage: kcdr-sender --help\n",
KCDR_VERSION);
		return 0;
		}
	}
	
	if (strlen(kcdr_f.pbx) == 0) {
		gethostname(kcdr_f.pbx, sizeof(kcdr_f.pbx));
		notice("pbx was set to hostname: %s", kcdr_f.pbx);
	}
	/* Send UDP packet */
	kcdr_sender_cfg *kcdr_cfg = get_kcdr_sender_cfg();
	debug("Send to: %s:%d", kcdr_cfg->host, kcdr_cfg->port);
	struct sockaddr_in si_other;
	int s;
	socklen_t slen = sizeof(si_other);
	char buf[BUFLEN];
	buf[0] = '\0';
	memcpy((void *)buf, (void *)(&kcdr_fh), sizeof(struct kcdr1_flow_header));
	memcpy((void *)(buf+sizeof(struct kcdr1_flow_header)), (void *)(&kcdr_f), sizeof(struct kcdr1_flow));

	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
		err("Can't open socket");

	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = htons(kcdr_cfg->port);
	if (inet_aton(kcdr_cfg->host, &si_other.sin_addr)==0) {
		err("inet_aton() failed");
		exit(1);
	}

	notice("Sending packet");

	if (sendto(s, buf, sizeof(struct kcdr1_flow_header) + sizeof(struct kcdr1_flow), 0, (struct sockaddr *)&si_other, slen)==-1)
		err("Can't send udp packet");
	else
		notice("Done!");

	close(s);
	free_kcdr_sender_cfg(kcdr_cfg);
	return 0;
}


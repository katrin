#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "def.h"
#include "traff.h"
#include "conf/conf.h"
#include "utils/utils.h"

#include "module/module_api.h"
#include "module/module_api_declaration.h"

static int
initial_node(struct paramlist **p, const char *param, int value) {
	debug("initial_node");
	debug("param = %s", param);
	debug("value = %d", value);
	(*p)->next = (struct paramlist *)malloc(sizeof(struct paramlist));
	if ((*p)->next == NULL) {
		err("malloc: can't allocate memory!");
		exit(EXIT_FAILURE);
	}
	
	(*p) = (*p)->next;
	
	strcpy((*p)->param, param);
	sprintf((*p)->value, "%d", value);
	(*p)->next = NULL;

	return 0;
}

void store_stat(struct user_traff_info *uTraffic) {
	kcfg *cfg = get_katrin_cfg();
	service_list *service = get_service_cfg(cfg, "traff");
	if (service == NULL)
			warn("Can't get traff service cfg");
	else {
		debug("service->store_null_cost_stat = %d", service->store_null_cost_stat);
		debug("uTraffic->cost = %1.5f", uTraffic->cost);
		if ( uTraffic->cost == 0 && service->store_null_cost_stat == 0 ) {
			debug("Don't save null cost traff");
			free_katrin_cfg(cfg);
			return;
		}
	}
	
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	/* Get params from uTraffic */
	struct paramlist *pl = NULL;
	struct paramlist *spl = NULL;

	// Add addr
	pl = (struct paramlist *)malloc(sizeof(struct paramlist));
	if (pl == NULL) {
		err("malloc: can't allocate memory!");
		exit(EXIT_FAILURE);
	}
	spl = pl;
	strcpy(pl->param, "addr");
	if (uTraffic->in_traff==INPUT_TRAFF) 
		sprintf(pl->value, "%u", ntohl(uTraffic->traffic.src_addr.s_addr));
	else
		sprintf(pl->value, "%u", ntohl(uTraffic->traffic.dst_addr.s_addr));
	
	// Add src port
	initial_node(&pl, "src_port", uTraffic->traffic.src_port);

	// Add dst port
	initial_node(&pl, "dst_port", uTraffic->traffic.dst_port);

	// Add bytes
	initial_node(&pl, "bytes", (int)uTraffic->traffic.bytes);

	// Add proto
	initial_node(&pl, "proto", (int)uTraffic->traffic.proto);

	// Add in_traff
	initial_node(&pl, "in_traff", uTraffic->in_traff);

	/* Store info to db */
	db.db_api.addstat("traff", uTraffic->userid, uTraffic->cost, spl);

	free_paramlist(spl);
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "def.h"
#include "tel.h"
#include "conf/conf.h"
#include "utils/utils.h"

#include "module/module_api.h"
#include "module/module_api_declaration.h"

void store_stat(struct user_tel_info *uTel) {
	kcfg *cfg = get_katrin_cfg();
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	/* Get params from uTel */
	struct paramlist *pl = NULL;
	struct paramlist *spl = NULL;

	// Add pbx
	pl = (struct paramlist *)malloc(sizeof(struct paramlist));
	spl = pl;
	strcpy(pl->param, "pbx");
	strcpy(pl->value, uTel->tel.pbx);
	
	// Add src_number
	pl->next = (struct paramlist *)malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "src_number");
	strcpy(pl->value, uTel->tel.src_number);
	
	// Add dst_number
	pl->next = (struct paramlist *) malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "dst_number");
	sprintf(pl->value, "%s", uTel->tel.dst_number);

	// Add src_trunk
	pl->next = (struct paramlist *) malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "src_trunk");
	sprintf(pl->value, "%s", uTel->tel.src_trunk);
	
	// Add dst_trunk
	pl->next = (struct paramlist *) malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "dst_trunk");
	sprintf(pl->value, "%s", uTel->tel.dst_trunk);

	// Add duration
	pl->next = (struct paramlist *) malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "duration");
	sprintf(pl->value, "%d",  uTel->tel.duration);

	// Add in_call
	pl->next = (struct paramlist *) malloc(sizeof(struct paramlist));
	pl = pl->next;
	strcpy(pl->param, "in_call");
	sprintf(pl->value, "%d", uTel->in_call);

	pl->next = NULL;

	/* Store info to db */
	db.db_api.addstat("tel", uTel->userid, uTel->cost, spl);

	free_paramlist(spl);
}


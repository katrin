/*
 * Copyright (C) 2007 Slava Semushin <php-coder@altlinux.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */


#ifndef COMPILER_H
#define COMPILER_H

/*
 * For more information about using attributes in GCC see:
 *
 * - http://www.unixwiz.net/techtips/gnu-c-attributes.html
 * - http://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html
 *
 **/


/* The attribute noreturn is not implemented in GCC versions earlier
 * than 2.5
 **/
#if defined(__GNUC__) && ___GNUC__ >= 2 && __GNUC_MINOR__ >= 5
	#define __noreturn  __attribute__((noreturn))
#else
	#define __noreturn
#endif


#ifdef __GNUC__
	#define __format(archetype, format_string, argument) \
		__attribute__((format(archetype, format_string, argument)))
#else
	#define __format(archetype, format_string, argument)
#endif


#endif /* COMPILER_H */

#ifndef DEF_INC
#define DEF_INC

#define BIN_DIR "/usr/bin"
#define KATRIN_LIB_DIR "/usr/lib/katrin"

#define TYPE_LENGTH	10

#define MAX_PARAM_NAME_LENGTH 15
#define MAX_PARAM_VALUE_LENGTH 25

struct tariff {
	int id;
	double perday;
	double permonth;
	char week_days[20];
};

struct user {
	int id;
	double balance;
	double credit;
	int tariffid;
	char password[15];
	int blocked;

	struct paramlist *uparams;
};

struct userlist {
	struct user user;
	struct userlist *next;
};

struct paramlist {
	char param[MAX_PARAM_NAME_LENGTH];
	char value[MAX_PARAM_VALUE_LENGTH];
	struct paramlist *next;
};

struct userslist {
	struct userslist *next;
	struct user user;
};

struct filter {
	int filterid;
	int tariffid;
	int priority;
	struct paramlist *params;
};

struct filterslist {
	struct filterslist *next;
	struct filter filter;
};

struct service_info {
		char type[TYPE_LENGTH];
};

struct user_service_info {
	int userid;
	double cost;
};

#endif


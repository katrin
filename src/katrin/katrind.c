#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <syslog.h>

#include <sys/resource.h>

#include "def.h"

#include "utils/utils.h"
#include "conf/conf.h"
#include "module/module_api_declaration.h"
#include "module/module_api.h"

void term_handler(int i);
int daemon_init(const char* name, int facility);
int set_signal_handlers(void);

int main(void) {
    /* Daemonization */
    daemon_init("katrind", LOG_DAEMON);

    /* Setting signal handlers map */
    set_signal_handlers();

	debug("katrind pid is %i\n", getpid());

    /* Get initial configuration */
    kcfg *cfg = get_katrin_cfg();

    //get_katrin_cfg();

	if (cfg == NULL) {
		return EXIT_FAILURE;
	}

	/* Startup */
	info("Start Katrin daemon");
	add_module(DB, cfg->db_mod);
	db.db_api.dbConnect();

	/*  Read service info from info collectors  */
	int pipedes[2];
	pipe(pipedes);

	char *info_mod = NULL;
	service_list *p = NULL;

    /* Loading business modules for each service */

	for (p = cfg->service_list ; p != NULL ; p = p->next) {
		debug("Service name: %s", p->name);
		add_module(BZ, p->name);
		
        list *l = NULL;
		
        for (l = p->info_mod; l != NULL ; l = l->next) {
			asprintf(&info_mod, "%s-%s", p->name, l->value);
			debug("Info module: %s", info_mod);

			add_module(INFO, info_mod, pipedes);

			free(info_mod);
		}
	}

	close(pipedes[1]);

    /* Main collecting cycle */

	char buf[1024];
	int len = 0;
	struct service_info *si = NULL;
    struct user_service_info *uService = NULL;

	while(1) {
        len = read(pipedes[0], buf, 1024);

        if(len != 0) {
            si = (struct service_info *)buf;
            
            debug("Service info was receive: %s", si->type);
            
            if (get_service_cfg(cfg, si->type) == NULL)
                continue;
            
            /* Send service info to libkatrin-bz-<type_service> */
            bz_module *bz_mod = get_bz_module(si->type);
            
            uService = bz_mod->bz_api.service2userService(si);
            
            debug("cost: %1.5f", uService->cost);
            
            if (uService->cost > 0)
                db.db_api.writeoffcost(uService->cost, uService->userid);
            
            free(uService);
        } else {
            err("Nothing was readed - break!");
            break;
        }
    }
    
    close(pipedes[0]);	
    
    free_katrin_cfg(cfg);
    return 0;
}

int daemon_init(const char* name, int facility) {
    pid_t pid;
    struct rlimit flim;
    int i = 0;

    if(getppid() != 1) {
        signal(SIGTTOU, SIG_IGN);
        signal(SIGTTIN, SIG_IGN);
        signal(SIGTSTP, SIG_IGN);
    }

    if((pid == fork()) != 0) {
        exit(0);
    }

    setsid();

    chdir("/");
    
    umask(0);
    
    getrlimit(RLIMIT_NOFILE, &flim);
    
    for(; i < flim.rlim_max; i++)
        close(i);
    
    openlog(name, LOG_PID, facility);

    return 0;
}

int set_signal_handlers(void) {
    signal(SIGTERM, term_handler);

    /*	
 	struct sigaction sa;
	sa.sa_handler = term_handler;
	sigaction(SIGTERM, &sa, 0);
    */
}

void term_handler(int signo) {
//	Kill all info modules
	debug("Kill all info modules");
	remove_all_info_modules();
	debug("Terminating");
	exit(EXIT_SUCCESS);
}


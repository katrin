#ifndef KCDR_H
#define KCDR_H
#include <sys/types.h>

struct kcdr1_flow_header {
	u_short		ver, count;
};

struct kcdr1_flow {
	char 		pbx[35];
	char		src_number[20], dst_number[20];
	char		src_trunk[15], dst_trunk[15];
	u_int		duration;
};

#endif

#ifndef UTILS_INC
#define UTILS_INC

#include <syslog.h>
#include "compiler.h"
#include "def.h"

enum fg_colors {
	RED = 31,
	GREEN,
	YELLOW,
	BLUE,
	VIOLET, /* not used */
	AZURE
};

void color_printf(int level, enum fg_colors color, char *format, ...) __format(printf, 3, 4);
char *get_param_value(const char *pname, struct paramlist *pl);
void paramlist2strlists(struct paramlist *params, char *field_list,
        char *value_list, char *middle_str_f, char *end_str_f,
        char *middle_str_v, char *end_str_v);
void free_paramlist(struct paramlist *pl);
int load_library(const char *libtype, const char *libname, void **libhandle);

#define debug(format...) color_printf(LOG_DEBUG, GREEN, format)
#define notice(format...) color_printf(LOG_NOTICE, BLUE, format)
#define info(format...) color_printf(LOG_INFO, AZURE, format)
#define warn(format...) color_printf(LOG_WARNING, YELLOW, format)
#define err(format...) color_printf(LOG_ERR, RED, format)

#endif


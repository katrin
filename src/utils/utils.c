#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <dlfcn.h>

#include "utils.h" /* for fg_colors enumeration */

void color_printf(int level, enum fg_colors color, char *format, ...) {
	printf("\033[%d;40m", color);

	va_list args;
	va_start (args, format);

	if (level != LOG_DEBUG && level != LOG_INFO) {
		char *str;

		vasprintf(&str, format, args);
		openlog("katrin", LOG_CONS | LOG_PID, LOG_DAEMON);
		syslog(level, str);
		
        closelog();
		free(str);
	}

	vprintf (format, args);

	va_end (args);

	puts("\033[0m");
}

char *get_param_value(const char *pname, struct paramlist *pl) {
//	debug("get_param_value");
	
	for ( ; pl != NULL; pl = pl->next) {
		if (strcmp(pname, pl->param) == 0) {
			return pl->value;
		}
	}
	
	debug("return NULL from get_param_value");
	return NULL;
}

void paramlist2strlists(struct paramlist *params, char *field_list, char *value_list, char *middle_str_f, char *end_str_f, char *middle_str_v, char *end_str_v) {
	debug("paramlist2strlists");
	int first = 1;
	struct paramlist *p = NULL;
	field_list[0] = '\0';
	value_list[0] = '\0';
	for (p = params; p!=NULL; p = p->next) {
		if (first==1) {
				strcat(field_list, end_str_f);
				strcat(value_list, end_str_v);
				first=0;
		}
		else {
				strcat(field_list, middle_str_f);
				strcat(value_list, middle_str_v);
		}
		debug("p->param = %s", p->param);
		strcat(field_list, p->param);
		strcat(field_list, end_str_f);
		
		debug("p->value = %s", p->value);
		strcat(value_list, p->value);
		strcat(value_list, end_str_v);
	}
}

void free_paramlist(struct paramlist *pl) {
	struct paramlist *item = NULL;
	struct paramlist *p = pl;

	while (p!=NULL) {
		item = p;
		p = p->next;
		free(item);
	}
}

/*
 * Build path to library and try to load it.
 * If library not loaded then returns 1.
 * If library successfully loaded then returns 0 and save library
 * hanlde in second argument.
 *
 * NOTE: after working with library handle you always should call
 *       dlclose() for it!
 *
 **/
int load_library(const char *libtype, const char *libname, void **libhandle) {
	int ret;
	char *path_to_lib;
	
	ret = asprintf(&path_to_lib, "%s/libkatrin-%s-%s.so", KATRIN_LIB_DIR, libtype, libname);
	if (ret == -1) {
		err("asprintf: can't create path for %s-%s library!", libtype, libname);
		return 1;
	}
	
	debug("Load library: %s", path_to_lib);
	
	(*libhandle) = dlopen(path_to_lib, RTLD_LAZY);
	free(path_to_lib);
	if ((*libhandle) == NULL) {
		fprintf(stderr, "%s\n", dlerror());
		return 1;
	}
	
	return 0;
}


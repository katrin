#ifndef TRAFF_INC
#define TRAFF_INC

#include <sys/types.h>
#include <netinet/in.h>

struct traff_info {
		/*  Binding attributes  */
		char type[TYPE_LENGTH];

		/* Special attributes  */
        struct in_addr	src_addr, dst_addr;
        uint16_t		src_port, dst_port;
        u_short			mib_in, mib_out;
        u_long			bytes;
        int				proto;
};

#define INPUT_TRAFF		1
#define OUTPUT_TRAFF	0

struct user_traff_info {
	/*  Binding attributes  */
	int userid;
	double cost;

	/* Special attributes  */
	struct traff_info traffic;
	
	/* 1 - input
	 * 0 - ouput  */
	int in_traff;
};

#endif


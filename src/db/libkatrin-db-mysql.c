#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <time.h>
#include "def.h"
#include "conf/conf.h"
#include "utils/utils.h"
#include "libkatrin-db-mysql.h"

//	Always connect for frequent query
void
dbConnect (void) {
	dbConnectMySQL(&mysql);
}

struct tariff *gettariff(int id) {
	debug("gettariff");
	char query[MAX_QUERY_SIZE];
	sprintf(query, "SELECT id,perday,permonth,week_days FROM tariffs WHERE id='%d';", id);
	struct tariff *p = NULL;
	MYSQL mysqllink;
	MYSQL_RES *res = NULL;
	MYSQL_ROW row = NULL;
	dbConnectMySQL(&mysqllink);
	
    if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 ) {
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	} else {
		res=mysql_store_result(&mysqllink);
		
        if (mysql_num_rows(res)==0) {
			debug("Can't get tariff!");
		} else {
			row = mysql_fetch_row(res);
			p = (struct tariff *) malloc(sizeof(struct tariff));
			p->id = atoi(row[0]);
			p->perday = atof(row[1]);
			p->permonth = atof(row[2]);
			assert(strlen(row[3]) < sizeof(p->week_days));
			strcpy(p->week_days, row[3]);
		}
	}

	mysql_free_result(res);
	
    dbDisconnectMySQL(&mysqllink);
	
    return p;
}

void
writeoffcost(double cost, int userid) {
	char query[MAX_QUERY_SIZE];
	sprintf(query,"update users set balance=balance-%1.4f where id='%d';",cost,userid);
	mysql_real_query(&mysql, query, strlen(query));
}

void set_block_user(int userid) {
	debug("set_block_user");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	sprintf(query,"UPDATE users SET blocked='Y' WHERE id='%d';", userid);
	mysql_real_query(&mysqllink, query, strlen(query));
}

int checkloginpassword(const char *login, const char *password) {
	debug("checkloginpassword");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	sprintf(query,"select id from users where login='%s' and password='%s';", login, password);
	mysql_real_query(&mysqllink, query, strlen(query));
	mysql_store_result(&mysqllink);
	if (mysql_affected_rows(&mysqllink)>0) {
		dbDisconnectMySQL(&mysqllink);
		return 1;
	}
	dbDisconnectMySQL(&mysqllink);
	return 0;
}

/* Return user struct finded by id or login if id=0 */
struct user *getuser(int id, char *login, char *typeService, char *uparam, char *uvalue) {
	debug("getuser");
	MYSQL_RES *res = NULL;
	MYSQL_ROW row = NULL;
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	char where[MAX_QUERY_SIZE];
	struct user *p = NULL;

	if (id!=0 || login!=NULL) {
		strcpy(query, "SELECT id,password,tariffid,balance,credit,blocked FROM users ");
		int usewhere = 0;
		if (id!=0) {
			sprintf(where," WHERE id='%d'", id);
			strcat(query, where);
			usewhere = 1;
		}
		
		if (login!=NULL) {
			if (usewhere==1)
				strcat(query, " AND");
			else
				strcat(query, " WHERE");
			sprintf(where," login='%s'", login);
			strcat(query, where);
			usewhere = 1;
		}

		debug(query);

		if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 ) {
			err("MySQL select query error: %s",mysql_error(&mysqllink));
		}
		else {
			res=mysql_store_result(&mysqllink);
			if (mysql_affected_rows(&mysqllink)==0) {
				p = NULL;
			}
			else {
				row=mysql_fetch_row(res);
				p = (struct user *) malloc(sizeof(struct user));
				p->id = atoi(row[0]);
				assert(strlen(row[1]) < sizeof(p->password));
				strcpy(p->password,row[1]);
				p->tariffid = atoi(row[2]);
				p->balance = atof(row[3]);
				p->credit = atof(row[4]);
				if (strcmp(row[5],"Y")==0)
					p->blocked = 1;
				else
					p->blocked = 0;
			}
		}
	}
		/*	Add uparams	*/
	if (typeService && uparam && uvalue) {
		sprintf(query,"SELECT * FROM service_%s_uparams WHERE %s='%s'", typeService, uparam, uvalue);
		debug(query);
		if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 ) {
			err("MySQL select query error: %s",mysql_error(&mysqllink));
		}
		else {
			res = mysql_store_result(&mysqllink);
			MYSQL_ROW_OFFSET saved_pos = mysql_row_tell(res);
			if (mysql_num_rows(res) > 0) {
				row = mysql_fetch_row(res);
				debug("userid=%s", row[1]);
				if (p==NULL)
					p = getuser(atoi(row[1]), NULL, NULL, NULL, NULL);
				mysql_row_seek(res, saved_pos);
				p->uparams = MySQLres2paramlist(res, 2);
			}
			else {
				debug("Can't get user with this param");
			}
		}
	}
	else {
		if (typeService)
			p->uparams = get_user_params(p->id, typeService);
		else
			if (p!=NULL)
				p->uparams = NULL;
	}

	mysql_free_result(res);
	dbDisconnectMySQL(&mysqllink);
	return p;
}

struct userslist *getusers4drop(void) {
	debug("getusers4drop");
	MYSQL_RES *res = NULL;
	MYSQL_ROW row = NULL;
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	int num_rows = 0;
	sprintf(query,"SELECT id FROM users WHERE balance<=-credit AND blocked='N';");
	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 )
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	else {
		res=mysql_store_result(&mysqllink);
		num_rows = mysql_affected_rows(&mysqllink);
	}
	struct userslist *p;
	struct userslist *ret_p = NULL;
	int i;
	for (i = 0; i < num_rows; i++) {
		row=mysql_fetch_row(res);
		if (i==0) {
			ret_p = (struct userslist *) malloc(sizeof(struct userslist));
			p = ret_p;
		}
		else {
			p->next = (struct userslist *) malloc(sizeof(struct userslist));
			p = p->next;
		}
		p->next = NULL;
		p->user.id = atoi(row[0]);
		debug("userid4drop: %d",p->user.id);
	}
	mysql_free_result(res);
	dbDisconnectMySQL(&mysqllink);
	return ret_p;
}

void addstat (char *typeService, int userid, double cost, struct paramlist *params)
{
	debug("addstat");
	time_t now = time(NULL);

	char query[MAX_QUERY_SIZE];

	char field_list[MAX_FIELD_LIST_SIZE];
	char value_list[MAX_VALUE_LIST_SIZE];
	paramlist2strlists(params, field_list, value_list, ",`", "`", ",'", "'");

	sprintf(query, "INSERT INTO service_%s_stats (`userid`, `storetime`, `cost`, %s) VALUES ('%d', FROM_UNIXTIME(%ld), %f, %s);", typeService, field_list, userid, now, cost, value_list);
	if (mysql_real_query(&mysql, query, strlen(query)) != 0 ) {
		err("MySQL insert query error: %s",mysql_error(&mysql));
		if (mysql_errno(&mysql) == CR_SERVER_GONE_ERROR)
			dbConnectMySQL(&mysql);
	}
	debug(query);
}

// Function return filters with tariffid or all filters if tariffid==0
struct filterslist *tariffid2filters (int tariffid, char *typeService)
{
	debug("tariffid2filters");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	MYSQL_RES *res = NULL;
	MYSQL_ROW row = NULL;
	char query[MAX_QUERY_SIZE];
	char where[50] = "\0";

	if (tariffid>0)
		sprintf(where,"WHERE tariffid=%d",tariffid);
	sprintf(query,"SELECT * FROM service_%s_filters %s order by priority;", typeService, where);

	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 ) {
		err("MySQL select query error: %s",mysql_error(&mysqllink));
		return NULL;
	}
	
	res = mysql_store_result(&mysqllink);
	
	int num_rows = mysql_affected_rows(&mysqllink);
	debug("num_rows: %d",num_rows);
	struct filterslist *p = NULL;
	struct filterslist *ret_p = NULL;
	int i = 0;

	for (i = 0; i<num_rows; i++) {
		if (i==0) {
                       	ret_p = (struct filterslist *) malloc(sizeof(struct filterslist));
                       	p = ret_p;
		}
		else {
			p->next = (struct filterslist *) malloc(sizeof(struct filterslist));
			p = p->next;
		}
		
		p->filter.params = NULL;	
		p->filter.params = MySQLres2paramlist(res, 3);

		row = mysql_fetch_row(res);
		p->next = NULL;
		p->filter.filterid = atoi(row[0]);
		p->filter.tariffid = atoi(row[1]);
		p->filter.priority = atoi(row[2]);
	}
    
    debug("num_rows: %d", num_rows);
    mysql_free_result(res);
    dbDisconnectMySQL(&mysqllink);
    return ret_p;
}

// DB service functions
void cleanstats(const char *startdate, const char *enddate) {
	debug("cleanstats");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	sprintf(query,"CALL cleanstats('%s','%s');",startdate,enddate);
	debug("query: %s", query);
	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 )
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	else
		mysql_affected_rows(&mysqllink);
	debug("startdate: %s",startdate);
	debug("enddate: %s",enddate);
	dbDisconnectMySQL(&mysqllink);
	return;
}

void sumstats(const char *startdate, const char *enddate) {
	debug("sumstats");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	sprintf(query,"CALL sumstats('%s','%s');",startdate,enddate);
	debug("query: %s",query);
	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 ) {
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	}
	else
		debug("sumstats done");
	dbDisconnectMySQL(&mysqllink);
	return;
}

// Function return users with tariffid or all users if tariffid==0
struct userslist *tariffid2users(int tariffid) {
	debug("tariffid2users");
	MYSQL_RES *res = NULL;
	MYSQL_ROW row = NULL;
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	char query[MAX_QUERY_SIZE];
	int num_rows = 0;
	sprintf(query,"SELECT id,ip,type FROM users WHERE tariffid='%d';",tariffid);
	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 )
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	else {
		res=mysql_store_result(&mysqllink);
		num_rows = mysql_affected_rows(&mysqllink);
	}
	struct userslist *p;
	struct userslist *ret_p = NULL;
	if (num_rows > 0) {
		int i;
		for (i = 0; i<num_rows ; i++) {
			row=mysql_fetch_row(res);
			if (i==0) {
				ret_p = (struct userslist *) malloc(sizeof(struct userslist));
				p = ret_p;
			}
			else {
				(*p).next = (struct userslist *) malloc(sizeof(struct userslist));
				p = (*p).next;
			}
			p->next = NULL;
			p->user.id = atoi(row[0]);
			
		}
	}
	mysql_free_result(res);
	dbDisconnectMySQL(&mysqllink);
	return ret_p;
}

struct paramlist *get_user_params(int userid, char *typeService) {
	debug("get_user_params");
	MYSQL mysqllink;
	dbConnectMySQL(&mysqllink);
	MYSQL_RES *res = NULL;
	char query[MAX_QUERY_SIZE];
	sprintf(query,"SELECT * FROM service_%s_uparams WHERE userid=%d;", typeService, userid);

	if (mysql_real_query(&mysqllink, query, strlen(query)) !=0 )
		err("MySQL select query error: %s",mysql_error(&mysqllink));
	else
		res = mysql_store_result(&mysqllink);

	struct paramlist *pl = MySQLres2paramlist(res, 2);
	mysql_free_result(res);
	dbDisconnectMySQL(&mysqllink);
	return pl;
}



// Additional specific function for mysql db

void
dbConnectMySQL(MYSQL *pmysqllink) {
	
	kcfg *cfg = get_katrin_cfg();

	if (cfg == NULL) {
		err("Can't get katrin configuration");
		return;
	}
	
	debug("Connect to DB...");
	mysql_init(pmysqllink);
	
	if (!(mysql_real_connect(pmysqllink, cfg->db_server, cfg->db_login, cfg->db_password, cfg->db_database, 0, NULL, 0)))
    {
        warn(" Error connection to DB: %s",mysql_error(pmysqllink));
        free_katrin_cfg(cfg);
        return;
    }
    else
        debug(" Good connect to DB");

	free_katrin_cfg(cfg);
}

void
dbDisconnectMySQL(MYSQL *pmysqllink) {
	mysql_close(pmysqllink);
}


struct paramlist *MySQLres2paramlist(MYSQL_RES *res, int start_fld) {
	debug("MySQLres2paramlist");
	if (mysql_num_rows(res) == 0) {
		debug("Null mysql res for convert fo paramlist!");
		return NULL;
	}
	MYSQL_ROW_OFFSET saved_pos = mysql_row_tell(res);
	MYSQL_ROW row = NULL;
	row = mysql_fetch_row(res);
	MYSQL_FIELD *fld;
	struct paramlist *pl = NULL;
	struct paramlist *tpl = NULL;
	struct paramlist *spl = NULL;
	int num_fields = mysql_num_fields(res);
	debug("All num fields: %d", num_fields);

	int j=0;
	for (j = start_fld; j < num_fields; j++) {
		/* XXX: Buffer overflow may occurs if row[j] >= MAX_PARAM_VALUE_LENGTH */
//		debug("row[%d]=%s", j, row[j]);
//		debug("strlen(row[%d])=%d",j,strlen(row[j]));

		assert(strlen(row[j]) < MAX_PARAM_VALUE_LENGTH);
			
		pl = (struct paramlist *) malloc(sizeof(struct paramlist));
		if (j==start_fld)
			spl = pl;
		else
			tpl->next = pl;
		fld = mysql_fetch_field_direct(res, j);
		strncpy(pl->param, fld->name, MAX_PARAM_NAME_LENGTH);
//		debug("fld->name=%s", fld->name);
		strncpy(pl->value, row[j], MAX_PARAM_VALUE_LENGTH);
		pl->next = NULL;
		tpl = pl;
	}
	mysql_row_seek(res, saved_pos);
	return spl;
}


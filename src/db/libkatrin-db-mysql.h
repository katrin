#ifndef LIBKATRIN_DB_MYSQL_INC
#define LIBKATRIN_DB_MYSQL_INC
#include <mysql/mysql.h>
#include <mysql/errmsg.h>
#include "db.h"

MYSQL mysql;
void dbConnectMySQL(MYSQL *pmysqllink);
void dbDisconnectMySQL(MYSQL *pmysqllink);
struct paramlist *MySQLres2paramlist(MYSQL_RES *res, int start_fld);
struct paramlist *get_user_params(int userid, char *typeService);
#define MAX_QUERY_SIZE  550
#define MAX_FIELD_LIST_SIZE	200
#define MAX_VALUE_LIST_SIZE	250

#endif


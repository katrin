#ifndef DB_INC
#define DB_INC
void dbConnect(void);

void writeoffcost(double cost,int userid);
void set_block_user(int userid);
int checkloginpassword(const char *login, const char *password);
struct userslist *login2user(char *login);
struct userslist *getusers4drop(void);
void setblockuser (int userid);
void addstat (char *typeService, int userid, double cost, struct paramlist *params);
struct filterslist *tariffid2filters (int tariffid, char *typeService);
void cleanstats(const char *startdate, const char *enddate);
void sumstats(const char *startdate, const char *enddate);
struct userslist *tariffid2users(int tariffid);
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/utils.h"
#include "conf/conf.h"
#include "compiler.h"

#include "module/module_api.h"
#include "module/module_api_declaration.h"

static void usage(void) __noreturn {
	printf("Usage:\n"
		"katrin-db-service cleanstats YYYY-MM-DD YYYY-MM-DD [--nullcost]\n"
		"katrin-db-service sumstats YYYY-MM-DD YYYY-MM-DD\n");
	exit(EXIT_FAILURE);
}

int main (int argc, char **argv) {

	kcfg *cfg = get_katrin_cfg();
	if (cfg == NULL) {
		return EXIT_FAILURE;
	}
	
	add_module(DB, cfg->db_mod);

	free_katrin_cfg(cfg);

	if (argc == 1) {
		usage();
	}

	if (strcmp(argv[1], "cleanstats") == 0) {
		debug("enter into clean section");

		if (argc - 1 < 2) {
			usage();
		}

		/* XXX:
		 * For prevent SQL-injection these arguments should be
		 * strict verified
		 * */
		db.db_api.cleanstats(argv[2],argv[3]);

	} else if (strcmp(argv[1], "sumstats") == 0) {
		debug("enter into sum section");

		if (argc - 1 < 3) {
			usage();
		}

		/* XXX:
		 * For prevent SQL-injection these arguments should be
		 * strict verified
		 * */
		db.db_api.sumstats(argv[2],argv[3]);

	} else {
		/* Unknown argument */
		usage();
	}
	return EXIT_SUCCESS;
}


#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "def.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "tel.h"

#include "module/module_api.h"
#include "module/module_api_declaration.h"

#include "stat/libkatrin-stat-tel.h"

struct user_tel_info *service2userService(struct tel_info *tel) {
	debug("service2userService in bz-tel module");
	struct user_tel_info *pUTI = (struct user_tel_info *) malloc(sizeof(struct user_tel_info));
	pUTI->tel = (*tel);
	//	Get info
	int tariffid = 0;

	kcfg *cfg = get_katrin_cfg();
	debug("Load db lib");
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	struct user *user = db.db_api.getuser(0, NULL, "tel", "tel_number", tel->dst_number);
	if (user == NULL) {
		user = db.db_api.getuser(0, NULL, "tel", "tel_number",  tel->src_number);
		if (user != NULL) {
			pUTI->in_call = OUTPUT_CALL;
		}
	}
	else {
		pUTI->in_call = INPUT_CALL;
	}

	if (user != NULL ) {
		tariffid = user->tariffid;
		pUTI->userid = user->id;
		free_paramlist(user->uparams);
		free(user);
	}
	else {
		pUTI->userid = 0;
		pUTI->cost = 0;
		return pUTI;
	}

	//	Apply filters
	int usefilter = 0;
	int applyfilter = 0;

	struct filterslist *filters = db.db_api.tariffid2filters(tariffid, "tel");
	struct filterslist *pfl = NULL;
	int st=1;
	if (filters!=NULL) {

		do {
			if (st==1)
				st=0;
			else
				filters=pfl;
			usefilter = 0;
			
			char *src_number = get_param_value("src_number", filters->filter.params);
			char *dst_number = get_param_value("dst_number", filters->filter.params);
			char *src_trunk = get_param_value("src_trunk", filters->filter.params);
			char *dst_trunk = get_param_value("dst_trunk", filters->filter.params);
			int in_call = atoi(get_param_value("in_call", filters->filter.params));
			double persecond_in = atof(get_param_value("persecond_in", filters->filter.params));
			double persecond_out = atof(get_param_value("persecond_out", filters->filter.params));

			// Check direction
			if (in_call==0 || in_call==pUTI->in_call) {
				debug("Apply direction of call");
				if (strlen(src_number) == 0 || strcmp(src_number, tel->src_number)) {
					debug("Apply src number");
					if (strlen(dst_number) == 0 || strcmp(dst_number, tel->dst_number)) {
						debug("Apply dst number");
						if (strlen(src_trunk) == 0 || strcmp(src_trunk, tel->src_trunk)) {
							debug("Apply src trunk");
							if (strlen(dst_trunk) == 0 || strcmp(dst_trunk, tel->dst_trunk)) {
								debug("Apply dst trunk");
								usefilter=1;
								debug("usefilter SET to 1!!!");
							}
						}
					}
				}
			}
			//Apply cost from first cheched filter and exit from check filters ELSE check next filter/
			if (usefilter==1 && applyfilter==0) {
				if (pUTI->in_call == INPUT_CALL)
					pUTI->cost = ((double)tel->duration)*persecond_in;
				if (pUTI->in_call == OUTPUT_CALL)
					pUTI->cost = ((double)tel->duration)*persecond_out;
				applyfilter = 1;
			}

			pfl = filters->next;
			free_paramlist(filters->filter.params);
			free(filters);
			
			if (pfl == NULL)
				debug("pfl == NULL");
			else
				debug("pfl != NULL");
		} while  (pfl!=NULL);
	}

	if (applyfilter == 0)
		pUTI->cost = 0;

	// Store stat before return rezult
	store_stat(pUTI);

	debug("exit from service2userService (tel bz module)");
	return pUTI;
}

int allowauth(char *login) {
	debug("allowauth");
	notice("allowauth");
	notice("login: %s", login);

	kcfg *cfg = get_katrin_cfg();
	debug("Load db lib");
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	struct user *user = db.db_api.getuser(0, login, NULL, NULL, NULL);
	struct tariff *tariff = db.db_api.gettariff(user->tariffid);
	notice("balace: %1.2f", user->balance);
	notice("credit: %1.2f", user->credit);
	int rezult = 0;
	if (user->balance + user->credit > 0 && user->blocked == 0) {
		//Prepare vars
		time_t t;
		struct tm *tmp;

		t = time(NULL);
		tmp = localtime(&t);
		if (tmp == NULL)
			warn("Can't get local time");

		//The hour as a decimal number using a 24-hour clock (range 00 to 23).
		char hour[3];
		strftime(hour, sizeof(hour), "%H", tmp);
		debug("hour: %s",hour);
		//The day of the week as a decimal, range 1 to 7, Monday being 1.
		char week_day[2];
		strftime(week_day, sizeof(week_day), "%u", tmp);
		debug("week_day: %s",week_day);
		//The day of the month as a decimal number (range 01 to 31).
		char month_day[3];
		strftime(month_day, sizeof(month_day), "%d", tmp);
		debug("month_day: %s",month_day);
		//The month as a decimal number (range 01 to 12).
		char year_month[3];
		strftime(year_month, sizeof(year_month), "%m", tmp);
		debug("year_month: %s",year_month);

		debug("tariff->week_days: %s",tariff->week_days);

		if (strlen(tariff->week_days)==0 || strstr(tariff->week_days,week_day)!=0)
			rezult = 1;
	}
	free(user);
	free(tariff);

	return rezult;

}


#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "def.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "traff.h"

#include "module/module_api.h"
#include "module/module_api_declaration.h"

#include "stat/libkatrin-stat-traff.h"

struct user_traff_info *service2userService(struct traff_info *traff) {
	debug("traffInfo2userTraffInfo");
	debug("Traff info struct:");
	debug("\ttraff->src_port = %d", traff->src_port);
	debug("\ttraff->proto = %d", traff->proto);
	
    char *s1;
	char *s2;
	
    s1 = strdup(inet_ntoa(traff->src_addr));
	s2 = strdup(inet_ntoa(traff->dst_addr));

    debug("\ttraff->src_addr = %s \n\ttraff->dst_addr = %s ", s1, s2);

	free(s1);
	free(s2);


	struct user_traff_info *pUTI = (struct user_traff_info *) malloc(sizeof(struct user_traff_info));
	pUTI->traffic = (*traff);
	//	Get info
	int tariffid = 0;
	int inputtraff = 0;
	int outputtraff = 0;

	struct user *user = db.db_api.getuser(0, NULL, "traff", "ip", inet_ntoa(traff->dst_addr));
	if (user == NULL) {
		user = db.db_api.getuser(0, NULL, "traff", "ip",  inet_ntoa(traff->src_addr));
		if (user != NULL) {
			outputtraff = 1;
			pUTI->in_traff = OUTPUT_TRAFF;
		}
	}
	else {
		inputtraff = 1;
		pUTI->in_traff = INPUT_TRAFF;
	}

	if (user != NULL ) {
		tariffid = user->tariffid;
		pUTI->userid = user->id;
		free_paramlist(user->uparams);
		free(user);
	}
	else {
		pUTI->userid = 0;
		pUTI->cost = 0;
		return pUTI;
	}

	//	Apply filters
	int usefilter = 0;
	int applyfilter = 0;

	struct filterslist *filters = db.db_api.tariffid2filters(tariffid, "traff");
	struct filterslist *pfl = NULL;
	int st=1;
	if (filters!=NULL) {
		//Prepare vars
		time_t t;
		struct tm *tmp;

		t = time(NULL);
		tmp = localtime(&t);
		if (tmp == NULL)
			warn("Can't get local time");

		//The hour as a decimal number using a 24-hour clock (range 00 to 23).
		char hour[3];
		strftime(hour, sizeof(hour), "%H", tmp);
		debug("hour: %s",hour);
		//The day of the week as a decimal, range 1 to 7, Monday being 1.
		char week_day[2];
		strftime(week_day, sizeof(week_day), "%u", tmp);
		debug("week_day: %s",week_day);
		//The day of the month as a decimal number (range 01 to 31).
		char month_day[3];
		strftime(month_day, sizeof(month_day), "%d", tmp);
		debug("month_day: %s",month_day);
		//The month as a decimal number (range 01 to 12).
		char year_month[3];
		strftime(year_month, sizeof(year_month), "%m", tmp);
		debug("year_month: %s",year_month);

		do {
			if (st==1)
				st=0;
			else
				filters=pfl;
			usefilter = 0;
			struct in_addr addr_mask;
			struct in_addr addr_ip;
			struct in_addr addr_net;

			char *network = get_param_value("network", filters->filter.params);
			char *netmask = get_param_value("netmask", filters->filter.params);
			int port = atoi(get_param_value("port", filters->filter.params));
			char *day_hours = get_param_value("day_hours", filters->filter.params);
			char *week_days = get_param_value("week_days", filters->filter.params);
			char *month_days = get_param_value("month_days", filters->filter.params);
			char *year_months = get_param_value("year_months", filters->filter.params);
			double permeginput = atof(get_param_value("permeginput", filters->filter.params));
			double permegoutput = atof(get_param_value("permegoutput", filters->filter.params));
			inet_aton(network, &addr_net);
			inet_aton(netmask, &addr_mask);

			debug("filters.network: %s filter.netmask: %s", 
							network,
							netmask);

			if (inputtraff==1)
				addr_ip = traff->src_addr;
			if (outputtraff==1)
				addr_ip = traff->dst_addr;
			//Check of conditions
			debug("Check net area...");
		if (((addr_ip.s_addr & addr_mask.s_addr) == addr_net.s_addr) || (strcmp(network,"")==0 || strcmp(netmask,"")==0) ) {
			debug("IP inside net area: %s %s",network, netmask);
			// Check port
			if (port==-1 || (port==traff->src_port && inputtraff==1) || (port==traff->dst_port && outputtraff==1)) {
				debug("Apply port filter");
				debug("filters.port=%d srcport=%d dstport=%d inputtraff=%d outputtraff=%d", 
								port, 
								traff->src_port, 
								traff->dst_port, 
								inputtraff, 
								outputtraff);
				//Check dey_hours
				debug("Check day_hours...");
				if (strcmp(day_hours, "")==0 || strstr(day_hours, hour)!=NULL) {
					debug("Hour inside day_hours: %s in %s", hour, day_hours);
					if (strcmp(week_days, "")==0 || strstr(week_days, week_day)!=NULL) {
						debug("Week_day inside : %s in %s",week_day, week_days);
						if (strcmp(month_days, "")==0 || strstr(month_days, month_day)!=NULL) {
							debug("Month_day inside : %s in %s", month_day, month_days);
							if (strcmp(year_months, "")==0 || strstr(year_months, year_month)!=NULL) {
								debug("Year_month inside : %s in %s", year_month, year_months);
// If _all_ check true, then for this traff use filter
							usefilter=1;
							debug("usefilter SET to 1!!!");
							}
						}
					}
				}
			}
		}
//Apply cost from first cheched filter and exit from check filters ELSE check next filter/
		if (usefilter==1 && applyfilter==0) {
			if (inputtraff == 1)
                                pUTI->cost = ((double)traff->bytes/1048576.0)*permeginput;
                        if (outputtraff == 1)
                                pUTI->cost = ((double)traff->bytes/1048576.0)*permegoutput;
			applyfilter = 1;
			}
		pfl = filters->next;
		free_paramlist(filters->filter.params);
		free(filters);

		if (pfl == NULL)
			debug("pfl == NULL");
		else
			debug("pfl != NULL");
                }
        while  (pfl!=NULL);
        }

	if (applyfilter == 0)
		pUTI->cost = 0;

	// Store stat before return result
	store_stat(pUTI);

	debug("exit from traffInfo2userTraffInfo");
	return pUTI;
}

int allowauth(char *login) {
	debug("allowauth");
	notice("allowauth");
	notice("login: %s", login);

	kcfg *cfg = get_katrin_cfg();
	debug("Load db lib");
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	struct user *user = db.db_api.getuser(0, login, NULL, NULL, NULL);
	struct tariff *tariff = db.db_api.gettariff(user->tariffid);
	notice("balace: %1.2f", user->balance);
	notice("credit: %1.2f", user->credit);
	int rezult = 0;
	if (user->balance + user->credit > 0 && user->blocked == 0) {
		//Prepare vars
		time_t t;
		struct tm *tmp;

		t = time(NULL);
		tmp = localtime(&t);
		if (tmp == NULL)
			warn("Can't get local time");

		//The hour as a decimal number using a 24-hour clock (range 00 to 23).
		char hour[3];
		strftime(hour, sizeof(hour), "%H", tmp);
		debug("hour: %s",hour);
		//The day of the week as a decimal, range 1 to 7, Monday being 1.
		char week_day[2];
		strftime(week_day, sizeof(week_day), "%u", tmp);
		debug("week_day: %s",week_day);
		//The day of the month as a decimal number (range 01 to 31).
		char month_day[3];
		strftime(month_day, sizeof(month_day), "%d", tmp);
		debug("month_day: %s",month_day);
		//The month as a decimal number (range 01 to 12).
		char year_month[3];
		strftime(year_month, sizeof(year_month), "%m", tmp);
		debug("year_month: %s",year_month);

		debug("tariff->week_days: %s",tariff->week_days);

		if (strlen(tariff->week_days)==0 || strstr(tariff->week_days,week_day)!=0)
			rezult = 1;
	}
	free(user);
	free(tariff);

	return rezult;

}


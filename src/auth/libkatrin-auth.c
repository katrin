#define _GNU_SOURCE
#include <stdio.h>

#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>

#include "def.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "module/module_api.h"
#include "module/module_api_declaration.h"

int (*authtypeuser)(char *login, char *password);

// Function for auth user, which check login&password in auth modules and db, get permission from bz module.
int authuser(char *login, char *password, char *type_service) {
	debug("authuser");
	int checklp=0;

	kcfg *cfg = get_katrin_cfg();
	if (cfg == NULL) {
		return 0;
	}

	/* Load libkatrin-bz-<type_service> */
	bz_module *bz = get_bz_module(type_service);
	if (bz == NULL) {
		debug("Can't get %s bz module. Load..", type_service);
		add_module(BZ, type_service);
	}

	
	debug("before call allowauth");
	if (bz->bz_api.allowauth(login)==1) {
		debug("ok allowauth");
		list *p;
		for (p = cfg->types_auth; p != NULL; p = p->next) {

			debug("type: %s", p->value);

			int ret;
			void *handleLibTypeAuth;
			
			/* Load lib for auth */
			ret = load_library("auth", p->value, &handleLibTypeAuth);
			if (ret) {
				free_katrin_cfg(cfg);
				return 0;
			}
			
			authtypeuser = dlsym(handleLibTypeAuth, "authtypeuser");
			if (authtypeuser(login,password) == 1) {
				checklp = 1;
			}
			
			dlclose(handleLibTypeAuth);
			
			if (checklp==1) {
				free_katrin_cfg(cfg);
				return 1;
			}
		}
	}
	free_katrin_cfg(cfg);
	return 0;
}


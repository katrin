#include "utils/utils.h"
#include "db/db.h"

#include "conf/conf.h"
#include "module/module_api.h"
#include "module/module_api_declaration.h"


int authtypeuser(char *user, char *password) {
	debug("authtypeuser [libkatrin-auth-db.c]");

	kcfg *cfg = get_katrin_cfg();
	debug("Load db lib");
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);

	if (db.db_api.checkloginpassword(user,password)==1) {
		debug("return 1 from libkatrin-auth-db.c");
		return 1;
	}

	return 0;
}

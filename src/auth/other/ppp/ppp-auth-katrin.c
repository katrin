#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <pppd/pppd.h>
#include <pppd/chap-new.h>
#include "auth/other/ppp/chap_ms.h"
#include <pppd/md5.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "auth/libkatrin-auth.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "module/module_api.h"
#include "module/module_api_declaration.h"

#define MAXLENPASSWORD 15
char global_login[15]="\0";
static int katrin_secret_check(void);
static int katrin_pap_auth(char *user,
			char *passwd,
			char **msgp,
			struct wordlist **paddrs,
			struct wordlist **popts);
static int katrin_chap_verify(char *login, char *ourname, int id,
			struct chap_digest_type *digest,
			unsigned char *challenge, unsigned char *response,
			char *message, int message_space);
static void katrin_ip_choose(u_int32_t *addrp);
static int katrin_allowed_address(u_int32_t addr);
char pppd_version[] = VERSION;

void
plugin_init(void)
{
notice("LOAD PPP PLUGIN");
pap_check_hook = katrin_secret_check;
chap_check_hook = katrin_secret_check;
pap_auth_hook = katrin_pap_auth;
chap_verify_hook = katrin_chap_verify;
ip_choose_hook = katrin_ip_choose;
allowed_address_hook = katrin_allowed_address;
}

static int
katrin_secret_check(void)
{
return 1;
}

static int
katrin_pap_auth(char *user,
                char *passwd,
                char **msgp,
                struct wordlist **paddrs,
                struct wordlist **popts)
{
	debug("katrin_pap_auth");
	
	struct user *pUser = db.db_api.getuser(0, user, "traff", NULL, NULL);
	if (pUser == NULL) {
		return 0;
	}

	char *ip = get_param_value("ip",pUser->uparams);
	size_t len = strlen(ip) + 1;
	
	*paddrs = (struct wordlist *) malloc(sizeof(struct wordlist) + len);
	if (*paddrs == NULL) {
		free(pUser);
		return 0;
	}
	
	(*paddrs)->word = (char*)(*paddrs + 1);
	(*paddrs)->next = NULL;
	BCOPY(ip, (*paddrs)->word, len);
	int rez = authuser(user, passwd, "traff");
	free(pUser);
	return rez;
}

#define MD5_HASH_SIZE    16
#define NEW_CHAP_FAILURE 0
#define NEW_CHAP_SUCCESS 1
static int katrin_chap_verify(char *login, char *ourname, int id,
			struct chap_digest_type *digest,
			unsigned char *challenge, unsigned char *response,
			char *message, int message_space)
{
	debug("katrin_chap_verify");
	notice("login: %s", login);
	/* XXX: Buffer overflow may occurs if login length >= 15 characters */
	assert(strlen(login) < sizeof(global_login));
	strcpy(global_login,login);
	char password[MAXLENPASSWORD] = "\0";
	struct user *user = NULL;
	user = db.db_api.getuser(0, login, NULL, NULL, NULL);
	if (user == NULL) {
		err("Can't get user struct");
		return 0;
	}
	/* XXX:
	* Buffer overflow may occurs if user->password length >= MAXLENPASSWORD
	* (15 characters)
	**/
	assert(strlen(user->password) < sizeof(password));
	strcpy(password,user->password);
	notice("DB_PASSWORD: %s",user->password);
	free(user);
	int challenge_len, response_len, password_len;
	int code = NEW_CHAP_FAILURE;

	MS_Chap2Response *rmd_2;
	MS_Chap2Response md_2;
	unsigned char saresponse[MS_AUTH_RESPONSE_LENGTH+1];
 
	password_len = strlen(password);
	challenge_len = *challenge++;
	response_len = *response++;
     
	switch (digest->code) {
		case CHAP_MICROSOFT_V2:		
			if (response_len != MS_CHAP2_RESPONSE_LEN) {
				slprintf(message, message_space, "E=691 R=1 C=%0.*B V=0", challenge_len, challenge);
				break;
			}
			rmd_2 = (MS_Chap2Response *) response;
			ChapMS2(challenge, rmd_2->PeerChallenge, login, password, password_len, &md_2, saresponse, MS_CHAP2_AUTHENTICATOR);
			if (memcmp(md_2.NTResp, rmd_2->NTResp, sizeof(md_2.NTResp)) == 0) {
				if (rmd_2->Flags[0])
					slprintf(message, message_space, "S=%s", saresponse);
				else
					slprintf(message, message_space, "S=%s M=%s", saresponse, "Access granted");
				code = NEW_CHAP_SUCCESS;
			}
			notice("CHAP_MICROSOFT_V2");
			notice("message: %s",message);
			break;
		default:
			err("Unknown CHAP method authentication!!!");
	}

	if(code != NEW_CHAP_SUCCESS)
		slprintf(message, message_space, "Access denied");
	return code;
}


static void katrin_ip_choose(u_int32_t *addrp) {
	debug("katrin_ip_choose");
	struct user *user = NULL;
	
	user = db.db_api.getuser(0, global_login, NULL, NULL, NULL);
	if (user == NULL) {
		return;
	}
	
	struct in_addr inpz;
	char *ip = get_param_value("ip", user->uparams);
	int check_ok = inet_aton(ip, &inpz);
	if(check_ok) {
		*addrp = inpz.s_addr;
	}
	free(user);
}

static int katrin_allowed_address(u_int32_t addr) {
	debug("katrin_allowed_address");
	add_module(BZ, "traff");
	bz_module *bz = get_bz_module("traff");
	int rez = bz->bz_api.allowauth(global_login);
	return rez;
}

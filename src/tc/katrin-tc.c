/* katrin-tc used to traffic control
 *
 * SYNOPSIS
 * katrin-tc <reload|remove> <iface> [userip]
 *
 * */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils/utils.h"
#include "conf/conf.h"

/* for inet_aton() */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* for chmod */
#include <sys/types.h>
#include <sys/stat.h>

#include "module/module_api.h"
#include "module/module_api_declaration.h"

#define ALL_RATE "100mbit"

/* Convert long netmask to short netmask
 * Example: 255.255.255.0 -> 24
 **/
static int lnm2snm(const char *lnm) {
	struct in_addr addr_mask;
	inet_aton(lnm, &addr_mask);
	int res = 0;
	int i = 0;
	uint32_t n = addr_mask.s_addr;
	for (i = 0; i < 32; i++)
	res += (n >> i & 1);
	
	return res;
}


int main (int argc, char **argv) {
	debug("Start katrin-tc");
	kcfg *cfg = get_katrin_cfg();
	if (cfg == NULL) {
		return EXIT_FAILURE;
	}
	debug("Load db lib");
	add_module(DB, cfg->db_mod);
	free_katrin_cfg(cfg);
	debug("argc: %d, argv[1]: %s",argc,argv[1]);
if (argc >= 3 && (strcmp(argv[1], "remove") == 0 || strcmp(argv[1], "reload") == 0)) {

	debug("iface: %s", argv[2]);
	debug("allrate: %s", ALL_RATE);

	int flowid=10;
	char command[200];

	int snm = 0;

	//open tmp script file
	char prog[] = "/tmp/katrin-tc.XXXXXX";
	mkstemp(prog);

	FILE *fh = fopen(prog,"wx");

	fwrite("#!/bin/sh\n", sizeof("#!/bin/sh\n")-1, 1, fh);

	sprintf(command, "tc qdisc del dev %s root handle 1: htb;\n", argv[2]);
	fwrite(command,strlen(command),1,fh);
	notice(command);
	if (strcmp(argv[1],"reload")==0) {
		sprintf(command, "tc qdisc add dev %s root handle 1: htb;\n", argv[2]);
		fwrite(command,strlen(command),1,fh);
		notice(command);
		sprintf(command, "tc class add dev %s parent 1: classid 1:1 htb rate %s;\n", argv[2], ALL_RATE);
		fwrite(command,strlen(command),1,fh);
		notice(command);
		struct filterslist *pfl = NULL;	
		pfl = db.db_api.tariffid2filters(0, "traff");
		struct userslist *pul = NULL;
		for (;pfl!=NULL;pfl=(*pfl).next) {
			debug("speedin: %s", get_param_value("speedin", pfl->filter.params));
			if (strcmp(get_param_value("speedin", pfl->filter.params), "")!=0) {
				debug("apply speed");
				//Prepare
				debug("netmask: %s", get_param_value("netmask", pfl->filter.params));
				snm = lnm2snm(get_param_value("netmask", pfl->filter.params));
				debug("new_netmask: %d",snm);
				if (argc==4) {
					flowid++;
					sprintf(command, "tc class add dev %s parent 1:1 classid 1:%d htb rate %s ceil %s;\n",
						argv[2], flowid, get_param_value("speedin", pfl->filter.params), get_param_value("speedin_max", pfl->filter.params));
					fwrite(command,strlen(command),1,fh);
					notice(command);
					sprintf(command, "tc filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip dst %s match ip src %s/%d flowid 1:%d;\n",
						argv[2], argv[3], get_param_value("network", pfl->filter.params), snm, flowid);
					fwrite(command,strlen(command),1,fh);
					notice(command);
				}
				else {
					//Apply to all users in the tariff.
					pul = db.db_api.tariffid2users(pfl->filter.tariffid);
					for (;pul!=NULL;pul=pul->next) {
						flowid++;
						sprintf(command, "tc class add dev %s parent 1:1 classid 1:%d htb rate %s ceil %s;\n",
							argv[2], flowid, get_param_value("speedin", pfl->filter.params), get_param_value("speedin_max", pfl->filter.params));
						fwrite(command,strlen(command),1,fh);
						notice(command);
						sprintf(command, "tc filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip dst %s match ip src %s/%d flowid 1:%d;\n",
						argv[2], get_param_value("ip",db.db_api.get_user_params(pul->user.id, "traff")), get_param_value("network", pfl->filter.params), snm, flowid);
						fwrite(command,strlen(command),1,fh);
						notice(command);
					}
				}
			}
		}
	}
	fclose(fh);

	int ret;
	ret = chmod(prog, S_IRUSR | S_IWUSR | S_IXUSR);
	if (ret == -1) {
		err("Can't do chmod(2) for temporary file!");
		unlink(prog);
		return EXIT_FAILURE;
	}

	if (fork() == 0)
		execl (prog, prog, NULL);
	unlink(prog);

}
else
info("Usage: katrin-tc <reload|remove> [iface userip]");
	
	return 0;
}


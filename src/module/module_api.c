#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include "utils/utils.h"

#include "module_api.h"
#include "module_api_declaration.h"

static int db_module_count = 0;
static int bz_module_count = 0;
static int info_module_count = 0;

static info_module info_module_array[INFO_MMAX];
static bz_module bz_module_array[BZ_MMAX];
db_module db;

int add_module(enum module_type type, const char *name, ...){
	va_list args;
	va_start(args, name);
	
	int res;
	int *pipedes;
	
	if (name == NULL) {
		err("Module name pointer is NULL!");
		return -1;
	}
	
	if (strlen(name) >= NAME_LENGTH) {
		err("Name is too long to be a module name!");
		return -1;
	}
	
	switch (type) {
		case DB:
			res = add_db_module(name);
			break;
		
		case BZ:
			res = add_bz_module(name);
			break;
		
		case INFO:
			pipedes = va_arg(args, int *);
			res = add_info_module(name, pipedes);
			break;
		
		default:
			err("No such module type!");
			return -1;
	}

    va_end(args);
	
	return res;
}

static int add_db_module(const char* name) {
	if(db_module_count > DB_MMAX){
		debug("DB already load");
		return 1;
	}
	
	memset(&db, 0, sizeof(db_module));
	
	int ret;
	
	ret = load_library("db", name, &(db.handleLibDB));
	if(ret){
		return 2;
	}
	
	strncpy(db.name,name, NAME_LENGTH);

	db.db_api.dbConnect = dlsym(db.handleLibDB,"dbConnect");
	db.db_api.writeoffcost = dlsym(db.handleLibDB,"writeoffcost");
	db.db_api.set_block_user = dlsym(db.handleLibDB,"set_block_user");
	db.db_api.checkloginpassword = dlsym(db.handleLibDB,"checkloginpassword");
	db.db_api.getuser = dlsym(db.handleLibDB,"getuser");
	db.db_api.gettariff = dlsym(db.handleLibDB,"gettariff");
	db.db_api.getusers4drop = dlsym(db.handleLibDB,"getusers4drop");
	db.db_api.addstat = dlsym(db.handleLibDB,"addstat");
	db.db_api.tariffid2filters = dlsym(db.handleLibDB,"tariffid2filters");
	db.db_api.cleanstats = dlsym(db.handleLibDB,"cleanstats");
	db.db_api.sumstats = dlsym(db.handleLibDB,"sumstats");
	db.db_api.tariffid2users = dlsym(db.handleLibDB,"tariffid2users");
	db.db_api.get_user_params = dlsym(db.handleLibDB,"get_user_params");
	
	if(db.db_api.dbConnect &&
		db.db_api.writeoffcost &&
		db.db_api.set_block_user &&
		db.db_api.checkloginpassword &&
		db.db_api.getuser &&
		db.db_api.gettariff &&
		db.db_api.getusers4drop &&
		db.db_api.addstat &&
		db.db_api.tariffid2filters &&
		db.db_api.cleanstats &&
		db.db_api.sumstats &&
		db.db_api.tariffid2users &&
		db.db_api.get_user_params) {
	
		db_module_count++;
	} else {
		err("No all db api funtions were define in %s db lib", name);
		dlclose(db.handleLibDB);
		return -2;
	}
	
	return 0;
}

static int add_bz_module(const char* name){
	int ret;
	
	if(bz_module_count > BZ_MMAX){
		err("No enough space in the array of bz modules");
		return 1;
	}

    /* Has module been already loaded? */
    //check_loaded_module( BZ, name);
	
	memset(&bz_module_array[bz_module_count], 0, sizeof(bz_module));
	
	ret = load_library("bz", name, &(bz_module_array[bz_module_count].handleLibBZ));
	if (ret) {
		return 2;
	}
	
	strncpy(bz_module_array[bz_module_count].name,name, NAME_LENGTH);
	
	bz_module_array[bz_module_count].bz_api.service2userService =
		dlsym(bz_module_array[bz_module_count].handleLibBZ,"service2userService");
	
	bz_module_array[bz_module_count].bz_api.allowauth =
		dlsym(bz_module_array[bz_module_count].handleLibBZ, "allowauth");
	
	if(bz_module_array[bz_module_count].bz_api.service2userService &&
		bz_module_array[bz_module_count].bz_api.allowauth) {
		
		bz_module_count++;
	} else {
		err("No all bz api funtions were define in %s bz lib", name);
		dlclose(bz_module_array[bz_module_count].handleLibBZ);
		return -1;
	}
	
	return 0;
}

static int add_info_module(const char* name, int *pipedes) {
    if(info_module_count < INFO_MMAX){
        memset(&info_module_array[info_module_count], 0, sizeof(info_module));

        strncpy(info_module_array[info_module_count].name,name, NAME_LENGTH);

		char *info_full_name;
		asprintf(&info_full_name, "%s/katrin-info-%s", BIN_DIR, name);
		debug("Load info module: %s", info_full_name);

		pid_t pid;
		pid = fork();
		if ( pid == 0 ) {
			close(pipedes[0]);
			dup2(pipedes[1], 1);

			execve(info_full_name, NULL, NULL);
			free(info_full_name);
			close(pipedes[1]);
		}

        if(pid > 0) {
			info_module_array[info_module_count].pid = pid;
            info_module_count++;
		}
        else {
            err("No all bz api funtions were define in %s.so", name);
            return -1;
        }
    } else {
        err("No enough space in the array of info modules");
        return 1;
    }

    return 0;
}

void remove_all_info_modules() {
	int i=0;
	for (i=0; i<info_module_count; i++) {
		debug("Kill %s info module", info_module_array[i].name);
		if (0 != kill(info_module_array[i].pid, SIGTERM))
			err("Can't kill %s info module", info_module_array[i].name);
	}
}

bz_module *get_bz_module(const char* name) {
	int i = 0;
	for (i=0; i < bz_module_count; i++) {
		if (strcmp(bz_module_array[i].name, name) == 0)
			return &bz_module_array[i];
	}
	return NULL;
}


#include <stdio.h>
#include "module_api.h"

int main(void){
    module_api *a = init_module_api();

    printf("%d\n", a->count(NULL));

    a->add(NULL, NULL);

    printf("%d\n", a->count(NULL));

    destroy_module_api(a);

    return 0;
}

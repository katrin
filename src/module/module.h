#ifndef _KATRIN_MODULE_H_
#define _KATRIN_MODULE_H_

#include "def.h"
#include "traff.h"

#define NAME_LENGTH 64

struct bz_interface {
    struct user_service_info* (*service2userService)(struct service_info *si);
    int (*allowauth)(char* login);

    int (*reload_conf)(void);
};

struct info_interface {
    int (*reload_conf)(void);
};

struct db_interface {
    void (*dbConnect)(void);
    void (*writeoffcost)(double cost,int userid);
    void (*set_block_user)(int userid);
    int (*checkloginpassword)(const char* login, const char* password);
    struct userslist* (*getusers4drop)(void);
	void (*addstat)(char *typeService, int userid, double cost, struct paramlist *params);	
    void (*setblockuser)(int userid);
    struct filterslist* (*tariffid2filters)(int tariffid, char *typeService);
    void (*cleanstats)(const char* startdate, const char* enddate);
    void (*sumstats)(const char* startdate, const char* enddate);
    struct userslist* (*tariffid2users)(int tariffid);
    struct user *(*getuser)(int id, char *login, char *typeService, char *uparam, char *uvalue);
    struct tariff* (*gettariff)(int id);
	struct paramlist *(*get_user_params)(int userid, char *typeService);

    int (*reload_conf)(void);
};

struct bz_module {
    char name[NAME_LENGTH];
    struct bz_interface bz_api;

    void* handleLibBZ;
};

struct info_module {
    char name[NAME_LENGTH];
    struct info_interface info_api;

	pid_t pid;
};

struct db_module {
    char name[NAME_LENGTH];
    struct db_interface db_api;

    void* handleLibDB;
};

struct module_api{
    int (*add)(const char* type, const char* name, ...);
    int (*remove)(const char* type, const char* name);
    int (*contain)(const char* type, const char* name);
    int (*reload)(const char* type, const char* name);
    int (*reload_conf)(const char* type);
    int (*count)(const char* type);
};

typedef struct db_module db_module;
typedef struct bz_module bz_module;
typedef struct info_module info_module;
typedef struct module_api module_api; 

#endif


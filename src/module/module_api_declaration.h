#ifndef _KATRIN_MODULE_API_DECLARATION_H_
#define _KATRIN_MODULE_API_DECLARATION_H_

#include "module/module.h"

enum module_type {
	DB,
	BZ,
	INFO
};

int add_module(enum module_type type, const char* name, ...);
void remove_all_info_modules();
bz_module *get_bz_module(const char* name);

static int add_db_module(const char* name);
static int add_bz_module(const char* name);
static int add_info_module(const char* name, int *pipedes);


#endif


#ifndef _KATRIN_MODULE_API_H_
#define _KATRIN_MODULE_API_H_

#include "module.h"

/* Maximum of info modules */
#define INFO_MMAX 32

/* Maximum of business modules */
#define BZ_MMAX 32

/* Maximum of database modules */
#define DB_MMAX 1

extern db_module db;

#endif


#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "conf-validate.h"
#include "utils/utils.h"
#include "conf/conf_info-tel-kcdr.h"

#define INFO_TEL_KCDR_CONF "/etc/katrin/info-tel-kcdr.conf"

/*
 * Some variable naming rule
 * User type - with _cfg postfix
 * libConfuse type - with _config postfix
 */

/* Getting configuration for kcdr telic module */
info_tel_kcdr_cfg *get_info_tel_kcdr_cfg(void) {
	info_tel_kcdr_cfg *cfg;
	cfg_t *info_tel_kcdr_config;

	cfg_opt_t info_tel_kcdr_opts[] = {
		CFG_INT("port", 9997, CFGF_NONE),
		CFG_STR("interface", "0.0.0.0", CFGF_NONE),
		CFG_END()
	};
	
	info_tel_kcdr_config = cfg_init(info_tel_kcdr_opts, CFGF_NONE);
	
	cfg_set_validate_func(info_tel_kcdr_config, "port", validate_port);
	cfg_set_validate_func(info_tel_kcdr_config, "interface", validate_interface);
	
	if (cfg_parse(info_tel_kcdr_config, INFO_TEL_KCDR_CONF) == CFG_PARSE_ERROR) {
		warn("Can't parse %s configuration file!", INFO_TEL_KCDR_CONF);
		goto free_info_tel_kcdr_config;
	}
	
	cfg = (info_tel_kcdr_cfg *)malloc(sizeof(info_tel_kcdr_cfg));
	if (cfg == NULL) {
		warn("Can't allocate memory for structure with katrin configuration!");
		goto free_info_tel_kcdr_config;
	}
	
	cfg->port = cfg_getint(info_tel_kcdr_config, "port");

	cfg->interface = strdup(cfg_getstr(info_tel_kcdr_config, "interface"));
	if (cfg->interface == NULL) {
		warn("Can't allocate memory for interface!");
		goto free_info_tel_kcdr_config;
	}

	cfg_free(info_tel_kcdr_config);

	return cfg;

free_info_tel_kcdr_config:
	cfg_free(info_tel_kcdr_config);
	return NULL;
}

void free_info_tel_kcdr_cfg(info_tel_kcdr_cfg *cfg) {
	free(cfg->interface);
	free(cfg);
}


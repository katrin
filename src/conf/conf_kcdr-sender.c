#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "conf-validate.h"
#include "utils/utils.h"
#include "conf/conf_kcdr-sender.h"

#define KCDR_SENDER_CONF "/etc/katrin/kcdr-sender.conf"

/*
 * Some variable naming rule
 * User type - with _cfg postfix
 * libConfuse type - with _config postfix
 */

/* Getting configuration for kcdr-sender */
kcdr_sender_cfg *get_kcdr_sender_cfg(void) {
	kcdr_sender_cfg *cfg;
	cfg_t *kcdr_sender_config;

	cfg_opt_t kcdr_sender_opts[] = {
		CFG_INT("port", 9997, CFGF_NONE),
		CFG_STR("host", "127.0.0.1", CFGF_NONE),
		CFG_END()
	};
	
	kcdr_sender_config = cfg_init(kcdr_sender_opts, CFGF_NONE);
	
	cfg_set_validate_func(kcdr_sender_config, "port", validate_port);
	cfg_set_validate_func(kcdr_sender_config, "host", validate_interface);
	
	if (cfg_parse(kcdr_sender_config, KCDR_SENDER_CONF) == CFG_PARSE_ERROR) {
		warn("Can't parse %s configuration file!", KCDR_SENDER_CONF);
		goto free_kcdr_sender_config;
	}
	
	cfg = (kcdr_sender_cfg *)malloc(sizeof(kcdr_sender_cfg));
	if (cfg == NULL) {
		warn("Can't allocate memory for structure with katrin configuration!");
		goto free_kcdr_sender_config;
	}
	
	cfg->port = cfg_getint(kcdr_sender_config, "port");

	cfg->host = strdup(cfg_getstr(kcdr_sender_config, "host"));
	if (cfg->host == NULL) {
		warn("Can't allocate memory for host!");
		goto free_kcdr_sender_config;
	}

	cfg_free(kcdr_sender_config);

	return cfg;

free_kcdr_sender_config:
	cfg_free(kcdr_sender_config);
	return NULL;
}

void free_kcdr_sender_cfg(kcdr_sender_cfg *cfg) {
	free(cfg->host);
	free(cfg);
}


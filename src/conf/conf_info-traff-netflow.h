#ifndef CONF_INFO_TRAFF_NETFLOW_H
#define CONF_INFO_TRAFF_NETFLOW_H

typedef struct info_traff_netflow_cfg {
    int port;
    char *interface;
} info_traff_netflow_cfg;

/* Katrin netflow config reading/erasing */
info_traff_netflow_cfg *get_info_traff_netflow_cfg(void);
void free_info_traff_netflow_cfg(info_traff_netflow_cfg *);

#endif

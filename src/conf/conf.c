/*
 * Copyright (C) 2007 Slava Semushin <php-coder@altlinux.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#define _GNU_SOURCE
#include <stdio.h>

#include <assert.h>
#include <confuse.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "conf-validate.h"
#include "utils/utils.h"

#define KATRIN_CONF "/etc/katrin/katrin.conf"
/*
 * Some variable naming rule
 * User type - with _cfg postfix
 * libConfuse type - with _config postfix
 */

static list *
create_item(const char *str) {
	list *item;

	assert(str != NULL);
	
	item = (list *)malloc(sizeof(list));
	if (item == NULL) {
		warn("Can't allocate memory for list structure!");
		return NULL;
	}
	
	item->value = strdup(str);
	if (item->value == NULL) {
		warn("Can't duplicate string: insufficient memory!");
		free(item);
		return NULL;
	}
	
	item->next = NULL;
	
	return item;
}

static void
free_item(list *item) {
	assert(item != NULL);

	free(item->value);
	free(item);
}

static void
free_list(list *first) {
	list *p;
	list *item;
	
	p = first;
	while (p != NULL) {
		item = p;
		p = p->next;
		free_item(item);
	}
}

static int
validate_seconds(cfg_t *cfg, cfg_opt_t *opt){
	int value = cfg_getint(cfg, opt->name);
	if (value < 0) {
		cfg_error(cfg, "Wrong value for '%s' option: can't be less then zero!", opt->name);
		return 1;
	}
	
	return 0;
}

kcfg *
get_katrin_cfg(void){
	unsigned int i;
	int ret;

	list *p;
	list *item;

	kcfg *cfg;
	cfg_t *katrin_config;

	/*
	* Here defaults for options are defined
	*/
	static cfg_opt_t service_opts[] = {
		CFG_STR_LIST("info_mod", "{}", CFGF_NONE),
		CFG_INT("store_null_cost_stat", 0, CFGF_NONE),
		CFG_END()
	};

	cfg_opt_t katrin_opts[] = {
		CFG_STR("db_mod", "mysql", CFGF_NONE),
		CFG_STR("db_server", "localhost", CFGF_NONE),
		CFG_STR("db_login", "katrin", CFGF_NONE),
		CFG_STR("db_password", "test123", CFGF_NONE),
		CFG_STR("db_database", "katrin", CFGF_NONE),
		CFG_STR_LIST("types_auth", "{db}", CFGF_NONE),
		CFG_INT("time_check_drop", 10, CFGF_NONE),
		CFG_SEC("service", service_opts, CFGF_MULTI | CFGF_TITLE),
		CFG_END()
	};
	
	katrin_config = cfg_init(katrin_opts, CFGF_NONE);
	
	cfg_set_validate_func(katrin_config, "time_check_drop", validate_seconds);
	
	if (cfg_parse(katrin_config, KATRIN_CONF) == CFG_PARSE_ERROR) {
		warn("Can't parse %s configuration file!", KATRIN_CONF);
		goto free_kcfg;
	}
	
	cfg = (kcfg *)malloc(sizeof(kcfg));
	if (cfg == NULL) {
		warn("Can't allocate memory for structure with katrin configuration!");
		goto free_kcfg;
	}
	
	ret = asprintf(&cfg->db_mod, "%s", cfg_getstr(katrin_config, "db_mod"));
	if (ret == -1)
		warn("Can't allocate memory for db_mod!");

	ret = asprintf(&cfg->db_server, "%s", cfg_getstr(katrin_config, "db_server"));
	if (ret == -1)
		warn("Can't allocate memory for db_server!");

	ret = asprintf(&cfg->db_login, "%s", cfg_getstr(katrin_config, "db_login"));
	if (ret == -1)
		warn("Can't allocate memory for db_login!");

	ret = asprintf(&cfg->db_password, "%s", cfg_getstr(katrin_config, "db_password"));
	if (ret == -1)
		warn("Can't allocate memory for db_password!");

	ret = asprintf(&cfg->db_database, "%s", cfg_getstr(katrin_config, "db_database"));
	if (ret == -1)
		warn("Can't allocate memory for db_database!");


	cfg->time_check_drop = cfg_getint(katrin_config, "time_check_drop");
	
	cfg->types_auth = NULL;
	
	p = cfg->types_auth;
	for (i = 0; i < cfg_size(katrin_config, "types_auth"); i++) {
		item = create_item(cfg_getnstr(katrin_config, "types_auth", i));
		if (item == NULL) {
			warn("Can't allocate memory for types_auth member!");
			goto free_types_auth;
		}
		
		if (p == NULL) {
			cfg->types_auth = item;
			p = item;
		} else {
			p->next = item;
			p = p->next;
		}
	}

	/* Add service config */
	unsigned n;
	int m, j;
	n = cfg_size(katrin_config, "service");
	service_list *sl = NULL;
	service_list *sl_w = NULL;
	service_list *sl_alloc = NULL;
	for(i = 0; i < n; i++) {
		sl_alloc = (service_list *) malloc(sizeof(service_list));
		if (i==0)
			sl = sl_alloc;
		else
			sl_w->next = sl_alloc;
		sl_w = sl_alloc;
		cfg_t *bm = cfg_getnsec(katrin_config, "service", i);
		strcpy(sl_w->name, cfg_title(bm));
		sl_w->store_null_cost_stat = cfg_getint(bm, "store_null_cost_stat");

		sl_w->info_mod = NULL;
		p = sl_w->info_mod;
		m = cfg_size(bm, "info_mod");
		for(j = 0; j < m; j++) {
			item = create_item(cfg_getnstr(bm, "info_mod", j));
			if (p == NULL) {
				sl_w->info_mod = item;
				p = item;
			} else {
				p->next = item;
				p = p->next;
			}
		}
		sl_w->next = NULL;
	}
	cfg->service_list = sl;


	
	cfg_free(katrin_config);
	return cfg;	

free_types_auth:
	free_list(cfg->types_auth);	
free_kcfg:
	cfg_free(katrin_config);
	return NULL;
}

service_list *get_service_cfg(kcfg *cfg, char *service_name) {
	service_list *service = NULL;
	for (service = cfg->service_list ; service->next != NULL ; service = service->next) {
		if (strcmp(service_name, service->name) == 0) {
			return service;
		}
	}
	return NULL;
}

void free_service_list(service_list *sl) {
	free_list(sl->info_mod);
	free(sl);
}

void free_katrin_cfg(kcfg *cfg){
	assert(cfg != NULL);
	
	free(cfg->db_mod);
	free(cfg->db_server);
	free(cfg->db_login);
	free(cfg->db_password);
	free(cfg->db_database);
	
	free_list(cfg->types_auth);

	service_list *sl;
	service_list *sl_t;
	for (sl = cfg->service_list ; sl != NULL ; sl = sl_t ) {
		sl_t = sl->next;
		free_service_list(sl);
	}
	
	free(cfg);
}



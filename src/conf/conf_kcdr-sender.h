#ifndef CONF_KCDR_SENDER_H
#define CONF_KCDR_SENDER_H

typedef struct kcdr_sender_cfg {
    int port;
    char *host;
} kcdr_sender_cfg;

/* kcdr-sender config reading/erasing */
kcdr_sender_cfg *get_kcdr_sender_cfg(void);
void free_kcdr_sender_cfg(kcdr_sender_cfg *);

#endif

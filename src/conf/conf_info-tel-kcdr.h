#ifndef CONF_INFO_TEL_KCDR_H
#define CONF_INFO_TEL_KCDR_H

typedef struct info_tel_kcdr_cfg {
    int port;
    char *interface;
} info_tel_kcdr_cfg;

/* Katrin kcdr config reading/erasing */
info_tel_kcdr_cfg *get_info_tel_kcdr_cfg(void);
void free_info_tel_kcdr_cfg(info_tel_kcdr_cfg *);

#endif

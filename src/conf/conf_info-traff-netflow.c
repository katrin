/*
 * Copyright (C) 2007 Mike Grozak <mike / tversu.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <confuse.h>
#include <stdlib.h>
#include <string.h>

#include "conf.h"
#include "conf-validate.h"
#include "utils/utils.h"
#include "conf/conf_info-traff-netflow.h"

#define INFO_TRAFF_NETFLOW_CONF "/etc/katrin/info-traff-netflow.conf"

/*
 * Some variable naming rule
 * User type - with _cfg postfix
 * libConfuse type - with _config postfix
 */

/* Getting configuration for netflow traffic module */
info_traff_netflow_cfg *get_info_traff_netflow_cfg(void) {
	info_traff_netflow_cfg *cfg;
	cfg_t *info_traff_netflow_config;

	cfg_opt_t info_traff_netflow_opts[] = {
		CFG_INT("port", 9996, CFGF_NONE),
		CFG_STR("interface", "0.0.0.0", CFGF_NONE),
		CFG_END()
	};
	
	info_traff_netflow_config = cfg_init(info_traff_netflow_opts, CFGF_NONE);
	
	cfg_set_validate_func(info_traff_netflow_config, "port", validate_port);
	cfg_set_validate_func(info_traff_netflow_config, "interface", validate_interface);
	
	if (cfg_parse(info_traff_netflow_config, INFO_TRAFF_NETFLOW_CONF) == CFG_PARSE_ERROR) {
		warn("Can't parse %s configuration file!", INFO_TRAFF_NETFLOW_CONF);
		goto free_info_traff_netflow_config;
	}
	
	cfg = (info_traff_netflow_cfg *)malloc(sizeof(info_traff_netflow_cfg));
	if (cfg == NULL) {
		warn("Can't allocate memory for structure with katrin configuration!");
		goto free_info_traff_netflow_config;
	}
	
	cfg->port = cfg_getint(info_traff_netflow_config, "port");

	cfg->interface = strdup(cfg_getstr(info_traff_netflow_config, "interface"));
	if (cfg->interface == NULL) {
		warn("Can't allocate memory for interface!");
		goto free_info_traff_netflow_config;
	}

	cfg_free(info_traff_netflow_config);

	return cfg;

free_info_traff_netflow_config:
	cfg_free(info_traff_netflow_config);
	return NULL;
}

void free_info_traff_netflow_cfg(info_traff_netflow_cfg *cfg) {
	free(cfg->interface);
	free(cfg);
}


/*
 * Copyright (C) 2007 Slava Semushin <php-coder@altlinux.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */


#ifndef CONF_H
#define CONF_H


typedef struct list {
	char *value;
	struct list *next;
} list;

typedef struct service_list{
	char name[15];
	list *info_mod;
	int store_null_cost_stat;
	struct service_list *next;
} service_list;

typedef struct katrin_cfg {
	char *db_mod;
	char *db_server;
	char *db_login;
	char *db_password;
	char *db_database;
	
	list *types_auth;

	service_list *service_list;
	
	int time_check_drop;
} kcfg;

/* Katrin config reading/erasing */
kcfg *get_katrin_cfg(void);
void free_katrin_cfg(kcfg *);
service_list *get_service_cfg(kcfg *cfg, char *service_name);

#endif /* CONF_H */

#ifndef CONF_VALIDATE_H
#define CONF_VALIDATE_H

#include <confuse.h>

int validate_port(cfg_t *cfg, cfg_opt_t *opt);
int validate_interface(cfg_t *cfg, cfg_opt_t *opt);

#endif

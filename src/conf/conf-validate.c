#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <confuse.h>

#include "conf-validate.h"
#include "utils/utils.h"

int validate_port(cfg_t *cfg, cfg_opt_t *opt) {
	int value = cfg_getint(cfg, opt->name);

	if(value > 65536 || value <= 0) {
		cfg_error(cfg, "Wrong value for '%s' option: must be between 1 and 65536", opt->name);
		return 1;
	}

	return 0;
}

int validate_interface(cfg_t *cfg, cfg_opt_t *opt) {
	struct in_addr net_ip;
	
	char *value = strdup(cfg_getstr(cfg, opt->name));
	if (value == NULL) {
		warn("Can't duplicate string: insufficient memory!");
		return 1;
	}
	
	
	if(inet_aton(value, &net_ip) == 0){
		cfg_error(cfg, "Wrong value for '%s' option: must be in standard numbers-and-dots notation", opt->name);
		free(value);
		return 1;
	}
	
	free(value);
	
	return 0;
}



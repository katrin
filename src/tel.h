#ifndef TEL_H
#define TEL_H

#include <sys/types.h>

struct tel_info {
		/*  Binding attributes  */
		char type[TYPE_LENGTH];

		/* Special attributes  */
		char		pbx[20];
        char		src_number[20], dst_number[20];
		char		src_trunk[15], dst_trunk[15];
        u_int		duration;
};

#define INPUT_CALL 1
#define OUTPUT_CALL	2

struct user_tel_info {
	/*  Binding attributes  */
	int userid;
	double cost;

	/* Special attributes  */
	struct tel_info tel;

	int in_call;
};

#endif


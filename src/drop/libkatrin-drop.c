#define _GNU_SOURCE
#include <stdio.h>

#include <stdlib.h>
#include <unistd.h>

#include "def.h"
#include "utils/utils.h"
#include "conf/conf.h"

#include "module/module_api.h"

/* Standart function for drop. Other special function placed in
 * drop program/scripts <type>-drop. Example ppp-drop
 **/

/* The dropuser function control how users will drop with depend by users.type
 **/
int dropuser(struct userlist *ul, char *typeService) {
	debug("dropuser");

	for (; ul != NULL; ul = ul->next) {

		char *drop_prog;
		int ret;
		char params[150];
		char values[150];
		paramlist2strlists(db.db_api.get_user_params(ul->user.id, typeService), params, values, " ", "", " ", "");
		ret = asprintf(&drop_prog, "%s/%s-drop %d %s", BIN_DIR, typeService, ul->user.id, values);
		if (ret == -1) {
			err("asprintf: can't write name of drop program to string!");
			return 1;
		}
		/* exec <type>-drop <ip> */
		debug("exec drop_prog: %s", drop_prog);
		if (system(drop_prog) != -1)
			db.db_api.set_block_user(ul->user.id);
		free(drop_prog);
		break;
	}

	return 0;
}

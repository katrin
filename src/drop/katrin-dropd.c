#include <unistd.h>
#include <stdlib.h>

#include "def.h"
#include "drop/libkatrin-drop.h"

#include "utils/utils.h"
#include "conf/conf.h"
#include "module/module_api.h"


// Function for prepare struct userslist for drop users
//and call dropusers function in libkatrin_drop module
static void check_drop(void) {
	debug("check_drop");
	struct userslist *p = NULL;
	struct userslist *item_p = NULL;

	p = db.db_api.getusers4drop();
	while (p!=NULL) {
		item_p = p;
		p=p->next;
		debug("user id for drop: %d", item_p->user.id);
		dropuser(*item_p);
		free(item_p);
	}
}

int main(void) {
	kcfg *cfg = get_katrin_cfg();
	if (cfg == NULL) {
		return EXIT_FAILURE;
	}

	//	Dynamic load modules and functions

	while(1) {
		check_drop();
		sleep(cfg->time_check_drop);
	}


	free_katrin_cfg(cfg);

	return EXIT_SUCCESS;
}


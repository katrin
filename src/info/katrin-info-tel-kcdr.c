#include <assert.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "def.h"
#include "tel.h"
#include "kcdr.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "conf/conf_info-tel-kcdr.h"
#include "katrin-info-tel-kcdr.h"

int main(void) {

	int sock;
	struct sockaddr_in addr;
	struct in_addr ip;
	char buf[CALL_FLOW_BUFFER_SIZE];
	int bytes_read = 0;

	/* Get options from config file */
	info_tel_kcdr_cfg *cfg = get_info_tel_kcdr_cfg();

	inet_aton(cfg->interface, &ip);

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(sock < 0) {
		err("Can't open socket");
		exit(1);
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(cfg->port);
	addr.sin_addr.s_addr = ip.s_addr;

	free_info_tel_kcdr_cfg(cfg);

/*	addr.sin_addr.s_addr = htonl(INADDR_ANY);   */

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		err("Can't bind port");
		exit(2);
	}

	char *p;
	int count=0;
	int kcdr_version = 0;
	int i=0;
	
	char *pbx;

	char *src_number;
	char *dst_number;

	char *src_trunk;
	char *dst_trunk;
	
	u_int duration;

	while(1) {
		bytes_read = recvfrom(sock, buf, CALL_FLOW_BUFFER_SIZE, 0, NULL, NULL);
//		debug("Packet received");
		buf[bytes_read] = '\0';
		p = buf;
		count = ((struct kcdr1_flow_header *)p)->count;
		kcdr_version = ((struct kcdr1_flow_header *)p)->ver;
//		debug("KCDR version: %d", kcdr_version);
//		debug("KCDR flow count: %d", count);
		switch (kcdr_version) {
			case 1:
				p+=sizeof(struct kcdr1_flow_header);
				for (i=0;i<count;i++) {
					if (i!=0)
						p+=sizeof(struct kcdr1_flow);
					pbx			= ((struct kcdr1_flow *)p)->pbx;
					src_number	= ((struct kcdr1_flow *)p)->src_number;
					dst_number	= ((struct kcdr1_flow *)p)->dst_number;
					src_trunk	= ((struct kcdr1_flow *)p)->src_trunk;
					dst_trunk	= ((struct kcdr1_flow *)p)->dst_trunk;
					duration	= ((struct kcdr1_flow *)p)->duration;
				}
				break;
			default:
//				notice("Unknown kcdr version");
				continue;
		}

		//Formating tel_info struct
		struct tel_info tel;
		strcpy(tel.type,"tel");

		assert(strlen(pbx) < sizeof(tel.pbx));

		assert(strlen(src_number) < sizeof(tel.src_number));
		assert(strlen(dst_number) < sizeof(tel.dst_number));

		strncpy(tel.pbx, pbx, sizeof(tel.pbx));
		strncpy(tel.src_number, src_number, sizeof(tel.src_number));
		strncpy(tel.dst_number, dst_number, sizeof(tel.dst_number));

		assert(strlen(src_trunk) < sizeof(tel.src_trunk));
		assert(strlen(dst_trunk) < sizeof(tel.dst_trunk));

		strncpy(tel.src_trunk, src_trunk, sizeof(tel.src_trunk));
		strncpy(tel.dst_trunk, dst_trunk, sizeof(tel.dst_trunk));

		tel.duration = duration;

		write(1, (void *)&tel, sizeof(struct tel_info));
	}
}


#ifndef LIBKATRIN_TI_NETFLOW_INC
#define LIBKATRIN_TI_NETFLOW_INC
#include <sys/types.h>
#include <netinet/in.h>

#define FLOW_BUFFER_SIZE 1024

struct flow_header {
	u_short         ver, count;
	u_long          up_time, cur_time_s, cur_time_n, sequence;
	u_char          trash[4];
	};
struct flow {
        struct in_addr  src_addr, dst_addr, next_addr;
        u_short         mib_in, mib_out;
        u_long          pkts, bytes, ts_first, ts_last;
        u_short         src_port, dst_port;
        u_char          pad, tcp_flags, proto, tos;
        u_short         src_as, dst_as;
        u_char          trash[4];
        };
#endif

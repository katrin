#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "katrin-info-traff-netflow.h"
#include "def.h"
#include "traff.h"
#include "utils/utils.h"
#include "conf/conf.h"
#include "conf/conf_info-traff-netflow.h"

int main(void) {

	int sock;
	struct sockaddr_in addr;
	struct in_addr ip;
	char buf[FLOW_BUFFER_SIZE];
	int bytes_read = 0;

	/* Get options from config file */
	info_traff_netflow_cfg *cfg = get_info_traff_netflow_cfg();

	inet_aton(cfg->interface, &ip);

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if(sock < 0) {
		err("Can't open socket");
		exit(1);
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(cfg->port);
	addr.sin_addr.s_addr = ip.s_addr;

	free_info_traff_netflow_cfg(cfg);

/*	addr.sin_addr.s_addr = htonl(INADDR_ANY);   */

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		err("Can't bind port");
		exit(2);
	}

	char *p;
	int count=0;
	int netflow_version = 0;
	int i=0;

	struct traff_info traff;

	while(1) {
		bytes_read = recvfrom(sock, buf, FLOW_BUFFER_SIZE, 0, NULL, NULL);
		buf[bytes_read] = '\0';
		p = buf;
		count = ntohs(((struct flow_header *)p)->count);
		netflow_version = ntohs(((struct flow_header *)p)->ver);
//		debug("Netflow version: %d", netflow_version);
		switch (netflow_version) {
			case 5:
				p+=sizeof(struct flow_header);
				for (i=0;i<count;i++) {
					if (i!=0)
						p+=sizeof(struct flow);

					//Formating traffInfo struct

					strcpy(traff.type,"traff");
					traff.bytes = ntohl(((struct flow *)p)->bytes);

					traff.src_addr = ((struct flow *)p)->src_addr;
					traff.dst_addr = ((struct flow *)p)->dst_addr;

					traff.src_port = ntohs(((struct flow *)p)->src_port);
					traff.dst_port = ntohs(((struct flow *)p)->dst_port);
					traff.proto = ((struct flow *)p)->proto;
					write(1, (void *)&traff, sizeof(struct traff_info));
				}
				break;
//			default:
//				notice("Unknown netflow version");
				continue;
		}


	}
}


%define katrin_user _katrin
%define katrin_group _katrin

Name: katrin
Version: 1.0.0
Release: alt1.RC4

Summary: Katrin - modular billing system.

License: GPL
Group: Monitoring
Url: http://katrin.sf.net

Source: %name-%version.tar
Packager: Denis Klimov <zver@altlinux.org>

BuildRequires: libconfuse-devel libMySQL-devel ppp-devel
Requires: libMySQL

%description
Modular billing system Katrin.
%description -l ru_RU.UTF8
Модульная биллинговая система Katrin.

%package monit
Summary: Monit config file for Katrin
Group: Monitoring
Requires: %name = %version-%release
Requires: monit
%description monit
Monit config file for Katrin
%description -l ru_RU.UTF8 monit
Конфигурационный файл monit для Katrin

%package -n kcdr-sender
Summary: Utility for send tel info by KCDR protocol.
Group: Networking/Other
%description -n kcdr-sender
Utility for send tel info by KCDR protocol.
%description -l ru_RU.UTF8 -n kcdr-sender
Утилита для отправки информации о телефонных звонках по протоколу KCDR.

%prep
%setup

%build
%autoreconf
%configure \
	--disable-static \
	--with-pppd-auth
%make_build

%install
%make_install DESTDIR=%buildroot install
install -pD -m 644 %name.sql %buildroot%_datadir/%name/%name.sql
install -pD -m 744 src/init.d/katrind %buildroot%_initdir/katrind
install -pD -m 744 src/init.d/katrin-dropd %buildroot%_initdir/katrin-dropd
install -pD -m 744 src/tc/ppp/katrin-tc-ppp.sh %buildroot%_sysconfdir/ppp/ip-up.d/katrin-tc-ppp.sh
install -pD -m 644 src/db/db-service/cron.d/katrin-db-sumstats %buildroot%_sysconfdir/cron.d/katrin-db-sumstats

install -pD -m 644 monitrc.d/katrind %buildroot%_sysconfdir/monitrc.d/katrind

%pre
%_sbindir/groupadd -r -f %katrin_group 2>/dev/null ||:
%_sbindir/useradd -g %katrin_group -c 'Katrin billing system' -d /var/empty -s '/dev/null' \
-r %katrin_user 2>/dev/null || :

%post
%post_service katrind
%post_service katrin-dropd

%preun
%preun_service katrind
%preun_service katrin-dropd

%post monit
%post_service monit

%postun monit
%post_service monit

%files
%doc AUTHORS THANKS
%_bindir/*
%attr(4710,root,%katrin_group) %_bindir/traff-ppp-drop
%exclude %_bindir/kcdr-sender
%_libdir/*
%_datadir/%name/
%_initdir/*
%_sysconfdir/ppp/ip-up.d/*
%_sysconfdir/cron.d/katrin-db-sumstats

%dir %_sysconfdir/%name
%config(noreplace) %attr(640,root,%katrin_group) %_sysconfdir/%name/*
%exclude %_sysconfdir/%name/kcdr-sender.conf

%files monit
%_sysconfdir/monitrc.d/katrind

%files -n kcdr-sender
%_bindir/kcdr-sender
%config(noreplace) %attr(640,root,root) %_sysconfdir/%name/kcdr-sender.conf

%changelog
* Thu Mar 27 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC4
- add monit subpackage

* Tue Mar 18 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC3
- fix memory leak
- add --with-pppd-auth option
- add store_null_cost_stat param to service

* Thu Mar 13 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC2
- fix work with service filters
- fix wrong asserts
- small fixes sql schema

* Tue Mar 04 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC1
- add autotools support

* Thu Feb 07 2008 Denis Klimov <zver@altlinux.ru> 1.0.0-alt1.RC0
- build for RC version

* Tue Dec 25 2007 Denis Klimov <zver@altlinux.ru> 0.12-alt1
- add config for netflow ti module
- add check blocked field in user struct
- add check netflow version

* Tue Dec 11 2007 Denis Klimov <zver@altlinux.ru> 0.11-alt1
- build with pkg-config
- add block for user which was drop

* Wed Dec 05 2007 Denis Klimov <zver@altlinux.ru> 0.10-alt1
- change db structure
- remove needless functions in code

* Tue Nov 27 2007 Denis Klimov <zver@altlinux.ru> 0.9.3-alt1
- fix Makefile and spec for build in x86_64 arch
- remove attr for libdir and bindir files

* Mon Nov 26 2007 Denis Klimov <zver@altlinux.ru> 0.9.2-alt1
- fix init scripts
- fix home dir for _katrin user

* Mon Nov 26 2007 Denis Klimov <zver@altlinux.ru> 0.9.1-alt1
- add _katrin user and group

* Thu Nov 22 2007 Denis Klimov <zver@altlinux.ru> 0.9-alt1
- change db structure
- add sumstats_month stored procedure

* Tue Nov 13 2007 Denis Klimov <zver@altlinux.ru> 0.8.2-alt1
- add stored procedure in db
- modify function sumstats in db module
- delete id field from stats table
- add new stats_cache table

* Fri Nov 09 2007 Denis Klimov <zver@altlinux.ru> 0.8.1-alt1
- build for 0.8.1

* Thu Nov 08 2007 Denis Klimov <zver@altlinux.ru> 0.8-alt1
- add auth by day of week
- fix memory leak

* Wed Oct 24 2007 Denis Klimov <zver@altlinux.ru> 0.7.3-alt1
- build for 0.7.3

* Thu Oct 18 2007 Denis Klimov <zver@altlinux.ru> 0.7.2-alt1
- build for 0.7.2

* Wed Oct 17 2007 Denis Klimov <zver@altlinux.ru> 0.7.1-alt1
- build for 0.7.1

* Sun Oct 07 2007 Denis Klimov <zver@altlinux.org> 0.7-alt1
- build for 0.7

* Thu Sep 06 2007 Denis Klimov <zver@altlinux.ru> 0.6-alt1
- add katrin-dropd daemon instead libkatrin-drop.so
- build for 0.6

* Wed Aug 22 2007 Denis Klimov <zver@altlinux.ru> 0.5.5-alt1
- build for 0.5.5

* Sun Aug 19 2007 Denis Klimov <zver@altlinux.org> 0.5.4-alt1
- build for 0.5.4

* Sun Aug 19 2007 Denis Klimov <zver@altlinux.org> 0.5.3-alt1
- build for 0.5.3

* Sat Aug 18 2007 Denis Klimov <zver@altlinux.org> 0.5.2-alt1
- build for 0.5.2

* Sat Aug 18 2007 Denis Klimov <zver@altlinux.org> 0.5.1-alt1
- build for 0.5.1

* Tue Aug 14 2007 Denis Klimov <zver@altlinux.ru> 0.5-alt1
- build for 0.5

* Fri Jul 13 2007 Denis Klimov <zver@altlinux.ru> 0.4.14-alt1
- install sql file into dir without version
- build for 0.4.14

* Thu Jul 12 2007 Denis Klimov <zver@altlinux.ru> 0.4.13-alt1
- build for 0.4.13

* Wed Jul 11 2007 Denis Klimov <zver@altlinux.ru> 0.4.12-alt1
- fix mode of init script
- build for 0.4.12

* Wed Jul 11 2007 Denis Klimov <zver@altlinux.ru> 0.4.11-alt1
- build for 0.4.11

* Fri Jul 06 2007 Denis Klimov <zver@altlinux.ru> 0.4.10-alt2
- change /etc/init.d to %%_initdir

* Wed Jul 04 2007 Denis Klimov <zver@altlinux.ru> 0.4.10-alt1
- add %%post and %%preun
- install init script

* Wed Jul 04 2007 Denis Klimov <zver@altlinux.ru> 0.4.9-alt1
- build for 0.4.9

* Fri Jun 29 2007 Denis Klimov <zver@altlinux.ru> 0.4.4-alt1
- build for 0.4.4

* Mon Jun 25 2007 Denis Klimov <zver@altlinux.ru> 0.4.3-alt1
- add config(noreplace) in files section
- build for 0.4.3

* Mon Jun 25 2007 Denis Klimov <zver@altlinux.ru> 0.4.2-alt1
- build for 0.4.2

* Thu Jun 21 2007 Denis Klimov <zver@altlinux.ru> 0.4-alt2
- fix install katrin.sql

* Tue Jun 19 2007 Denis Klimov <zver@altlinux.ru> 0.4-alt1
- build for 0.4

* Sun Jun 17 2007 Denis Klimov <zver@altlinux.org> 0.3.7-alt1
- build for 0.3.7

* Sat Jun 09 2007 Denis Klimov <zver@altlinux.org> 0.3.6-alt1
- build for 0.3.6

* Fri Jun 08 2007 Denis Klimov <zver@altlinux.ru> 0.3.4-alt1
- build for 0.3.4

* Thu May 31 2007 Denis Klimov <zver@altlinux.ru> 0.2.2-alt1
- add requires
- add katrin.sql

* Thu May 31 2007 Denis Klimov <zver@altlinux.ru> 0.2-alt1
- build for 0.2 version

* Wed Mar 28 2007 Denis Klimov <zver@altlinux.org> 0.1-alt1
- initial build


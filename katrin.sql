-- MySQL dump 10.11
--
-- Host: localhost    Database: katrin_rc
-- ------------------------------------------------------
-- Server version	5.0.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `service_tel_filters`
--

DROP TABLE IF EXISTS `service_tel_filters`;
CREATE TABLE `service_tel_filters` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `tariffid` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned NOT NULL,
  `src_number` varchar(20) NOT NULL,
  `dst_number` varchar(20) NOT NULL,
  `src_trunk` varchar(15) NOT NULL,
  `dst_trunk` varchar(15) NOT NULL,
  `in_call` tinyint(4) NOT NULL,
  `persecond_in` float NOT NULL,
  `persecond_out` float NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `service_tel_stats`
--

DROP TABLE IF EXISTS `service_tel_stats`;
CREATE TABLE `service_tel_stats` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL,
  `storetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `cost` float NOT NULL,
  `pbx` varchar(20) NOT NULL,
  `src_number` varchar(20) NOT NULL,
  `dst_number` varchar(20) NOT NULL,
  `src_trunk` varchar(15) NOT NULL,
  `dst_trunk` varchar(15) NOT NULL,
  `in_call` tinyint(1) NOT NULL,
  `duration` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `service_tel_uparams`
--

DROP TABLE IF EXISTS `service_tel_uparams`;
CREATE TABLE `service_tel_uparams` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL,
  `access_type` varchar(15) NOT NULL,
  `tel_number` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `service_traff_filters`
--

DROP TABLE IF EXISTS `service_traff_filters`;
CREATE TABLE `service_traff_filters` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `tariffid` int(11) unsigned NOT NULL default '0',
  `priority` int(10) unsigned NOT NULL default '0',
  `network` char(20) NOT NULL default '0.0.0.0',
  `netmask` char(20) NOT NULL default '0.0.0.0',
  `port` int(10) NOT NULL default '-1',
  `proto` int(10) NOT NULL default '-1',
  `permeginput` float unsigned NOT NULL default '0',
  `permegoutput` float unsigned NOT NULL default '0',
  `speedin` char(10) default '',
  `speedin_max` char(10) default '',
  `speedout` char(10) default '',
  `speedout_max` char(10) default '',
  `day_hours` char(50) default '',
  `week_days` char(20) default '',
  `month_days` char(60) default '',
  `year_months` char(40) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `service_traff_stats`
--

DROP TABLE IF EXISTS `service_traff_stats`;
CREATE TABLE `service_traff_stats` (
  `userid` int(11) unsigned NOT NULL default '0',
  `storetime` timestamp NOT NULL default '0000-00-00 00:00:00',
  `cost` float unsigned NOT NULL default '0',
  `addr` int(4) unsigned default NULL,
  `src_port` int(2) unsigned default NULL,
  `dst_port` int(2) unsigned default NULL,
  `bytes` int(11) unsigned NOT NULL,
  `proto` int(1) unsigned default NULL,
  `in_traff` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `service_traff_stats_cache`
--

DROP TABLE IF EXISTS `service_traff_stats_cache`;
CREATE TABLE `service_traff_stats_cache` (
  `storetime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `bytes` bigint(8) unsigned NOT NULL default '0',
  `userid` int(10) unsigned NOT NULL,
  `in_traff` tinyint(1) NOT NULL default '0',
  `cost` double unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `service_traff_uparams`
--

DROP TABLE IF EXISTS `service_traff_uparams`;
CREATE TABLE `service_traff_uparams` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL,
  `access_type` varchar(15) NOT NULL default '',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `tariffs`
--

DROP TABLE IF EXISTS `tariffs`;
CREATE TABLE `tariffs` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` char(40) NOT NULL,
  `perday` float NOT NULL,
  `permonth` float NOT NULL,
  `week_days` char(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `login` char(80) NOT NULL,
  `password` char(20) NOT NULL,
  `lastname` char(20) NOT NULL,
  `firstname` char(20) NOT NULL,
  `middlename` char(20) NOT NULL,
  `tariffid` int(11) unsigned NOT NULL default '0',
  `balance` float NOT NULL default '0',
  `credit` float unsigned NOT NULL default '0',
  `blocked` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping routines for database 'katrin_rc'
--

DELIMITER ;;
DROP PROCEDURE IF EXISTS `cleanstats`;;
CREATE PROCEDURE `cleanstats`(IN startdate CHAR(20), IN enddate CHAR(20))
BEGIN
               DELETE FROM service_traff_stats WHERE storetime BETWEEN startdate AND enddate;
END ;;

DROP PROCEDURE IF EXISTS `sumstats`;;
CREATE PROCEDURE `sumstats`(IN startdate CHAR(20), IN enddate CHAR(20))
BEGIN
	DECLARE userid_int, b INT;
	DECLARE sum_bytes BIGINT;
	DECLARE sum_cost DOUBLE;
	DECLARE in_traff_int tinyint(1);
	DECLARE cur_1 CURSOR FOR SELECT sum(bytes), sum(cost), userid, in_traff FROM service_traff_stats WHERE storetime BETWEEN startdate AND enddate GROUP BY userid, in_traff;
	DECLARE CONTINUE HANDLER FOR NOT FOUND
		SET b=1;
	OPEN cur_1;
	REPEAT
		FETCH cur_1 INTO sum_bytes, sum_cost, userid_int, in_traff_int;
		IF userid_int IS NOT NULL THEN
			DELETE FROM service_traff_stats_cache WHERE storetime = startdate AND userid = userid_int AND in_traff = in_traff_int;
			INSERT INTO service_traff_stats_cache ()VALUES (startdate, sum_bytes, userid_int, in_traff_int, sum_cost);
		END IF;
		UNTIL b=1
	END REPEAT;
    CLOSE cur_1;
END;;

DROP PROCEDURE IF EXISTS `sumstats_month` ;;
CREATE PROCEDURE `sumstats_month`(IN year CHAR(4), IN month CHAR(2))
BEGIN
	DECLARE day, day_next INT;
	SET day = 1;
	WHILE day<31 DO
		SET day_next = day + 1;
		CALL sumstats(CONCAT(year,'-',month,'-',day,' 00:00:00'), CONCAT(year,'-',month,'-',day_next,' 00:00:00'));
		SET day = day + 1;
	END WHILE;
END ;;

DELIMITER ;

-- Dump completed on 2008-03-06 12:04:20
